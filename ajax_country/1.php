<?php 
try{
     $connection = new mysqli('localhost','root','','nepal');
      $sql = "select * from provinces";
      $result = $connection->query($sql);
      $prov = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($prov, $row);
        }
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }
 ?>
 <!DOCTYPE html>
 <html>
 <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title></title>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 </head>
 <body>
 Provience:<select name="province_id" id="Province">
        <option value="">Select Province</option>
        <?php foreach($prov as $province){ ?>
          <option value="<?php echo $province->ID ?>" 
            ><?php echo $province->province ?></option>
        <?php } ?>
    </select>
District: <select id="district-dropdown"><option value="">Select District</option></select>

<script>
$(document).ready(function() {
    $('#Province').on('change', function() {
        var province_id = this.value;
        $.ajax({
            url: "district.php",
            type: "POST",
            data: {
                province_id: province_id
            },
            success: function(result){
                $("#district-dropdown").html(result);
            }
        });
    });
});    
</script>
 </body>
 </html>