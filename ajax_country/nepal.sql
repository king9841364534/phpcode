-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2022 at 07:35 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nepal`
--

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `ID` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `district` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`ID`, `province_id`, `district`) VALUES
(1, 1, 'Bhojpur District'),
(2, 1, 'Dhankuta District'),
(3, 1, 'Ilam District'),
(4, 1, 'Jhapa District'),
(5, 1, 'Khotang District'),
(6, 1, 'Udayapur District'),
(7, 1, 'Morang District'),
(8, 1, 'Okhaldhunga District'),
(9, 1, 'Panchthar District'),
(10, 1, 'Sankhuwasabha District'),
(11, 1, 'Solukhumbu District'),
(12, 1, 'Sunsari District'),
(13, 1, 'Taplejung District'),
(14, 1, 'Tehrathum District'),
(15, 2, 'Parsa District'),
(16, 2, 'Bara District'),
(17, 2, 'Rautahat District'),
(18, 2, 'Sarlahi District'),
(19, 2, 'Dhanusha District'),
(20, 2, 'Siraha District'),
(21, 2, 'Mahottari District'),
(22, 2, 'Saptari District'),
(23, 3, 'Sindhuli District'),
(24, 3, 'Ramechhap District'),
(25, 3, 'Dolakha District'),
(26, 3, 'Bhaktapur District'),
(27, 3, 'Dhading District'),
(28, 3, 'Kathmandu District'),
(29, 3, 'Kavrepalanchok District'),
(30, 3, 'Lalitpur District'),
(31, 3, 'Nuwakot District'),
(32, 3, 'Rasuwa District'),
(33, 3, 'Sindhupalchok District'),
(34, 3, 'Chitwan District'),
(35, 3, 'Makwanpur District'),
(36, 4, 'Baglung District'),
(37, 4, 'Gorkha District'),
(38, 4, 'Kaski District'),
(39, 4, 'Lamjung District'),
(40, 4, 'Manang District'),
(41, 4, 'Mustang District'),
(42, 4, 'Myagdi District'),
(43, 4, 'Nawalpur District'),
(44, 4, 'Parbat District'),
(45, 4, 'Syangja District'),
(46, 4, 'Tanahun District'),
(47, 5, 'Kapilvastu District'),
(48, 5, 'Parasi District'),
(49, 5, 'Rupandehi'),
(50, 5, 'Arghakhanchi District'),
(51, 5, 'Gulmi District'),
(52, 5, 'Palpa District'),
(53, 5, 'Dang District'),
(54, 5, 'Pyuthan District'),
(55, 5, 'Rolpa District'),
(56, 5, 'Eastern Rukum'),
(57, 5, 'Banke District'),
(58, 5, 'Bardiya District'),
(59, 6, 'Western Rukum District'),
(60, 6, 'Salyan District'),
(61, 6, 'Dolpa District'),
(62, 6, 'Humla District'),
(63, 6, 'Jumla District'),
(64, 6, 'Kalikot District'),
(65, 6, 'Mugu District'),
(66, 6, 'Surkhet District'),
(67, 6, 'Dailekh District'),
(68, 6, 'Jajarkot District'),
(69, 7, 'Kailali District'),
(70, 7, 'Achham District'),
(71, 7, 'Doti District'),
(72, 7, 'Bajhang District'),
(73, 7, 'Bajura District'),
(74, 7, 'Kanchanpur District'),
(75, 7, 'Dadeldhura District'),
(76, 7, 'Baitadi District'),
(77, 7, 'Darchula District');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `ID` int(11) NOT NULL,
  `province` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`ID`, `province`) VALUES
(1, 'Province No. 1'),
(2, 'Madhesh Province'),
(3, 'Bagmati Province'),
(4, 'Gandaki Province'),
(5, 'Lumbini Province'),
(6, 'Karnali Province'),
(7, 'Sudurpashchim Province');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `province_id` (`province_id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `foreign key` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
