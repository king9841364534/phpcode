<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<pre>
<?php
	$fruits = ['Banana' , 'Mango' , 'Apple' , 'Chery'];
	echo $fruits[0];
	echo '<br>' . $fruits[2];
	$fruits[] = 'Grapes';
	echo '<br>' . $fruits[4];
	$fruits[2] = 'Pappya';
	echo '<br>' . $fruits[2];
	var_dump($fruits); 
	print_r($fruits);

	unset($fruits[1]);
	echo '<br>' . $fruits[1];
	var_dump($fruits); 
	print_r($fruits);

	$info = [55 => 12 , 'Ram' , 20 , 56.5 , 'ram@gmail.com' , false]; //=> implies
	//die('test');
	var_dump($info); 
	print_r($info);
	echo 'My email is ' . $info[59];
	//git clone url to clone code from git
	//Associative index array
	$info = ['s.n' => 20, 'roll' => 12 , 'name' => 'Ram' , 'age' => 20 , 'weight' => 56.5 , 'email' => 'ram@gmail.com'];
	var_dump($info); 
	print_r($info);
	echo 'My name is ' . $info['name'];
 ?>
 <table border='1' width="30%">
		<tr>
			<th>S.N</th>
			<td><?php echo $info['s.n']; ?></td>
		</tr>
		<tr>
			<th>Roll</th>
			<td><?php echo $info['roll']; ?></td>
		</tr>
		<tr>
			<th>Name</th>
			<td><?php echo $info['name']; ?></td>
		</tr>
		<tr>
			<th>Age</th>
			<td><?php echo $info['age']; ?></td>
		</tr>
		<tr>
			<th>Weight</th>
			<td><?php echo $info['weight']; ?></td>
		</tr>
		<tr>
			<th>Email</th>
			<td><?php echo $info['email']; ?></td>
		</tr>
	</table>
</body>
</html>
