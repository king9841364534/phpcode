<?php require_once 'header.php'; 
$vacancies->set('id',$_GET['id']);
$vacancies_detail = $vacancies->getVacancyById();
  if(!$vacancies_detail){
    header('location:index.php');
  }
$applicant->set('vacancy_id',$vacancies_detail[0]->id);
if(isset($_POST['btnSubmit'])){
    $applicant->set('name',$_POST['name']);
    $applicant->set('email',$_POST['email']);
    if(array_key_exists('cv_file', $_FILES) && array_key_exists('error', $_FILES['cv_file'])){
        $upload_cv_pdf=uniqid() . '_' .$_FILES["cv_file"]["name"];
        move_uploaded_file($_FILES["cv_file"]["tmp_name"],"Company/uploads/" . $upload_cv_pdf);
        $applicant->set('cv_file',$upload_cv_pdf);
    }
    if(array_key_exists('cl_file', $_FILES) && array_key_exists('error', $_FILES['cl_file'])){
        
        $upload_cl_pdf=uniqid() . '_' .$_FILES["cl_file"]["name"];
        move_uploaded_file($_FILES["cl_file"]["tmp_name"],"Company/uploads/" . $upload_cl_pdf);
        $applicant->set('cl_file',$upload_cl_pdf);
    }
    $applicant->saveApplicantDetails();
  }
?>


        <!-- Header End -->
        <div class="container-xxl py-5 bg-dark page-header mb-5">
            <div class="container my-5 pt-5 pb-4">
                <h1 class="display-3 text-white mb-3 animated slideInDown">Job Detail</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb text-uppercase">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item text-white active" aria-current="page">Job Detail</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- Header End -->


        <!-- Job Detail Start -->
        <div class="container-xxl py-5 wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row gy-5 gx-4">
                    <div class="col-lg-8">
                        <div class="d-flex align-items-center mb-5">
                            <img class="flex-shrink-0 img-fluid border rounded" src="company/img/<?php echo $vacancies_detail[0]->image ?>" alt="" style="width: 80px; height: 80px;">
                            <div class="text-start ps-4">
                                <h3 class="mb-3"><?php echo $vacancies_detail[0]->title ?></h3>
                                <span class="text-truncate me-3"><i class="fa fa-map-marker-alt text-primary me-2"></i><?php echo $vacancies_detail[0]->address ?></span>
                                <span class="text-truncate me-3"><i class="far fa-clock text-primary me-2"></i><?php echo $vacancies_detail[0]->jobnature ?></span>
                                <span class="text-truncate me-0"><i class="far fa-money-bill-alt text-primary me-2"></i><?php if(empty($vacancies_detail[0]->salaries)){ echo 'Negotiable'; }else{ echo 'Rs. '.$vacancies_detail[0]->salaries; }?></span>
                            </div>
                        </div>

                        <div class="mb-5">
                            <h4 class="mb-3">Job description</h4>
                            <p><?php echo $vacancies_detail[0]->job_description ?></p>
                            <h4 class="mb-3">Responsibility</h4>
                            <p><?php echo $vacancies_detail[0]->responsibilities ?></p>
                            <h4 class="mb-3">Requirements</h4>
                            <p><?php echo $vacancies_detail[0]->requirement ?></p>
                            <h4 class="mb-3">Address</h4>
                            <div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $vacancies_detail[0]->map ?>"><a href="https://www.gps.ie/farm-gps/">tractor gps</a></iframe></div>
                        </div>
        
                        <div class="">
                            <h4 class="mb-4">Apply For The Job</h4>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="row g-3">
                                    <div class="col-12 col-sm-12">
                                        <label class="form-group">Name:</label>
                                        <input type="text" name="name" class="form-control" placeholder="Your Name" required=''>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <label class="form-group">Email:</label>
                                        <input type="email" name="email" class="form-control" placeholder="Your Email" required=''>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <label class="form-group">Curriculum Vitae(only PDF):</label>
                                        <input type="file" name="cv_file" id="cv_file" class="form-control bg-white" required='' accept="application/pdf">
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <label class="form-group">Cover Letter(only PDF):</label>
                                        <input type="file" name="cl_file" id="cl_file" class="form-control bg-white" required='' accept="application/pdf">
                                    </div>
                                    <div class="col-12">
                                        <button class="btn btn-primary w-100" name="btnSubmit" type="submit">Apply Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
        
                    <div class="col-lg-4">
                        <div class="bg-light rounded p-5 mb-4 wow slideInUp" data-wow-delay="0.1s">
                            <h4 class="mb-4">Job Summery</h4>
                            <p><i class="fa fa-angle-right text-primary me-2"></i>Published On: <?php echo date("jS M Y", strtotime($vacancies_detail[0]->date_line)) ?></p>
                            <p><i class="fa fa-angle-right text-primary me-2"></i>Vacancy: <?php echo $vacancies_detail[0]->require_no_of_employee ?> Position</p>
                            <p><i class="fa fa-angle-right text-primary me-2"></i>Job Nature: <?php echo $vacancies_detail[0]->jobnature ?></p>
                            <p><i class="fa fa-angle-right text-primary me-2"></i>Salary: <?php if(empty($vacancies_detail[0]->salaries)){ echo 'Negotiable'; }else{ echo 'Rs. '.$vacancies_detail[0]->salaries; }?></p>
                            <p><i class="fa fa-angle-right text-primary me-2"></i>Location: <?php echo $vacancies_detail[0]->addres ?></p>
                            <p class="m-0"><i class="fa fa-angle-right text-primary me-2"></i>Date Line: <?php echo date("jS M Y", strtotime($vacancies_detail[0]->date_line)) ?></p>
                        </div>
                        <div class="bg-light rounded p-5 wow slideInUp" data-wow-delay="0.1s">
                            <h4 class="mb-4"><?php echo $vacancies_detail[0]->name ?> Detail</h4>
                            <p class="m-0"><?php echo $vacancies_detail[0]->description ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Job Detail End -->


<?php require_once 'footer.php' ?>