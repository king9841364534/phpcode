<?php require_once 'header.php' ?>
        <!-- Carousel Start -->
        <div class="container-fluid p-0">
            <div class="owl-carousel header-carousel position-relative">
                <div class="owl-carousel-item position-relative">
                    <img class="img-fluid" src="img/carousel-1.jpg" alt="">
                    <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(43, 57, 64, .5);">
                        <div class="container">
                            <div class="row justify-content-start">
                                <div class="col-10 col-lg-8">
                                    <h1 class="display-3 text-white animated slideInDown mb-4">Find The Perfect Job That You Deserved</h1>
                                    <a href="" class="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft">Search A Job</a>
                                    <a href="" class="btn btn-secondary py-md-3 px-md-5 animated slideInRight">Find A Talent</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="owl-carousel-item position-relative">
                    <img class="img-fluid" src="img/carousel-2.jpg" alt="">
                    <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(43, 57, 64, .5);">
                        <div class="container">
                            <div class="row justify-content-start">
                                <div class="col-10 col-lg-8">
                                    <h1 class="display-3 text-white animated slideInDown mb-4">Find The Best Startup Job That Fit You</h1>
                                    <p class="fs-5 fw-medium text-white mb-4 pb-2">Vero elitr justo clita lorem. Ipsum dolor at sed stet sit diam no. Kasd rebum ipsum et diam justo clita et kasd rebum sea elitr.</p>
                                    <a href="" class="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft">Search A Job</a>
                                    <a href="" class="btn btn-secondary py-md-3 px-md-5 animated slideInRight">Find A Talent</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Carousel End -->

        <!-- Category Start -->
        <div class="container-xxl py-5">
            <div class="container">
                <h1 class="text-center mb-5 wow fadeInUp" data-wow-delay="0.1s">Explore By Category</h1>
                <div class="row g-4">
                    <?php foreach ($categories_list as $category) { ?>
                    <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.1s">
                            <a class="cat-item rounded p-4" href="job-list.php?id=<?php echo $category->id ?>">
                            <img src="Admin/img/<?php echo $category->logo ?>" height="54">
                            <h6 class="mb-3"><?php echo $category->title ?></h6>
                            <p class="mb-0"><?php $vacancies->set('c_id',$category->id); echo count($vacancies->listVacancyByCategory()); ?> Vacancy</p>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- Category End -->

        <!-- Jobs Start -->
        <div class="container-xxl py-5">
            <div class="container">
                <h1 class="text-center mb-5 wow fadeInUp" data-wow-delay="0.1s">Job Listing</h1>
                <div class="tab-class text-center wow fadeInUp" data-wow-delay="0.3s">
                    <ul class="nav nav-pills d-inline-flex justify-content-center border-bottom mb-5">
                        <?php foreach($jobNature_list as $in =>$jobNature) { ?>
                        <li class="nav-item"><?php if($in == 0){ ?>
                            <a class="d-flex align-items-center text-start mx-3 me-0 pb-3 active" data-bs-toggle="pill" href="#tab-<?php echo $in+1 ?>"> <?php }else { ?>
                                <a class="d-flex align-items-center text-start mx-3 me-0 pb-3" data-bs-toggle="pill" href="#tab-<?php echo $in+1 ?>">
                            <?php } ?>
                                <h6 class="mt-n1 mb-0"><?php echo $jobNature->title ?></h6>
                            </a>
                        </li>
                        <?php } ?> 
                    </ul>
                    <div class="tab-content">
                        <?php foreach ($jobNature_list as $in =>$jobNature) { ?><?php if($in == 0){ ?>
                        <div id="tab-<?php echo $jobNature->id; ?>" class="tab-pane fade show p-0 active"> <?php }else { ?><div id="tab-<?php echo $jobNature->id; ?>" class="tab-pane fade show p-0 "> <?php } ?>
                            <div class="job-item p-4 mb-4">
                                <div class="row g-4"><?php $vacancies->set('job_nature',$jobNature->id); $vacancies_list = $vacancies->listVacancyByNature(); if($vacancies_list == null){?><h5 class="text-danger">No data found</h5><?php }else{ foreach ($vacancies_list as $vacancy) { ?>
                                    <div class="col-sm-12 col-md-8 d-flex align-items-center">
                                        <?php if(!empty($vacancy->image)){?>
                                        <img class="flex-shrink-0 img-fluid border rounded" src="company/img/<?php echo $vacancy->image ?>" alt="" style="width: 80px; height: 80px;"><?php }else { ?>
                                        <img class="flex-shrink-0 img-fluid border rounded"  src="Company/img/dummy-company.png" alt="" style="width: 80px; height: 80px;">
                                        <?php } ?>
                                        <div class="text-start ps-4">
                                            <h5 class="mb-3"><?php echo $vacancy->title ?></h5>
                                            <span class="text-truncate me-3"><i class="fa fa-map-marker-alt text-primary me-2"></i><?php echo $vacancy->address ?></span>
                                            <span class="text-truncate me-3"><i class="far fa-clock text-primary me-2"></i><?php echo $vacancy->jobnature ?></span>
                                            <span class="text-truncate me-0"><i class="far fa-money-bill-alt text-primary me-2"></i><?php if(empty($vacancy->salaries)){ echo 'Negotiable'; }else{ echo 'Rs. '.$vacancy->salaries; }?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 d-flex flex-column align-items-start align-items-md-end justify-content-center">
                                        <div class="d-flex mb-3">
                                            <a class="btn btn-primary" href="job-detail.php?title=<?php echo $vacancy->title ?>&id=<?php echo $vacancy->id ?>">Apply Now</a>
                                        </div>
                                        <small class="text-truncate"><i class="far fa-calendar-alt text-primary me-2"></i>Date Line: <?php echo date("jS M Y", strtotime($vacancy->date_line)) ?></small>
                                    </div> <?php } }?>
                                </div>
                            </div>
                            <a class="btn btn-primary py-3 px-5" href="job-list.php">More Jobs</a>
                        </div>
                     <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Jobs End -->
<?php require_once 'footer.php' ?>

