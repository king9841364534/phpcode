<?php
$email = $password = ''; 

    if(isset($_POST['btnSubmit'])){
        $error = [];

        if(isset($_POST['email']) && !empty($_POST['email']) && trim($_POST['email'])){
            $email = $_POST['email'];
            if(!filter_var($email , FILTER_VALIDATE_EMAIL)){
                $error['email'] = "Please enter a valid email";
                echo "<script>alert('Please enter a valid email');</script>";
            }
        }else{
            $error['email'] = "Please enter a valid email";
            echo "<script>alert('Please enter a valid email');</script>";
        }

        if(isset($_POST['password']) && !empty($_POST['password'])){
            $password = $_POST['password'];
        }else{
            $error['password'] = "Please provide a password";
            echo "<script>alert('Please provide a password');</script>";
        }

        if(count($error)==0){
            require_once '../admin/library/Company.php';
            $db = new Company();
            $user = $db->checkCompanyForLogin($email,md5($password)); 

            if($user){
                if($user->new_user == 0){
                session_start();
                $_SESSION['user_name'] = $user->name;
                $_SESSION['user_email'] = $user->emial;
                $_SESSION['user_id'] = $user->id;
                header('location:dashboard.php?id='.$user->id);
            }elseif($user->new_user == 1){
                session_start();
                $_SESSION['user_name'] = $user->name;
                $_SESSION['user_email'] = $user->emial;
                $_SESSION['user_id'] = $user->id;
                header('location:edit_profile.php?id='.$user->id);
            }
            }else{
               $error['loginfail'] = "Email or Password doesn't match";
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>JobPortal Company - Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">


    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <style type="text/css">
        .error {
            color: #ff5a02;
            font-size: 1rem;
            line-height: 1;
            width: 100%;
        }
    </style>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>

                                        <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                            <p class="alert alert-danger">Please login to continue</p>
                                          <?php } ?>

                                          <?php if (isset($error['loginfail'])) { ?>
                                            <p class="alert alert-danger"><?php  echo $error['loginfail']; ?></p>
                                          <?php } ?>
                                    </div>
                                    <form class="user" id="loginForm" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                                        <div class="form-group">
                                            <input type="email" name="email" value="<?php echo $email ?>" class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Enter Email Address..." required>
                                            <span class="error">
                                                <?php echo (isset($error['email'])?$error['email']:'') ?>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user"
                                                id="exampleInputPassword" placeholder="Password" required>
                                            
                                            <span class="error">
                                                <?php echo (isset($error['password'])?$error['password']:'') ?>
                                            </span>
                                        </div>

                                        <div class="form-group">
                                            <input type="submit" name="btnSubmit" class="btn btn-primary btn-user btn-block">
                                        </div>
                                    </form>
                                    <hr>
                                    <div>
                                        <a class="small" href="register.php">Don't have an account? Register!</a>
                                    </div>
                                    <div class="row">
                                        <div class="col-8">
                                        </div>
                                        <div class="col-4">
                                            <a href="../index.php" class="btn btn-success">Home</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#loginForm").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 5
                    },
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    email: "Please enter a valid email address"
                }
            });
        });
    </script>
    </script>

</body>

</html>