<?php
    $title="Edit Company-JobPortal"; 
    require_once '../admin/library/company.php';
    $company = new Company();
    $id = $_GET['id'];  
    $locations = $company->getLocation();
    $company->set('id',$id);
    if(isset($_POST['btnUpdate'])){
        $company->name=$_POST['name'];
        $company->size=$_POST['size'];
        if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
            $types= ['image/png','image/jpeg','image/jpg','image/gif'];
            if(in_array($_FILES['image']['type'],$types)){
                $image = uniqid() . '_' .$_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $image);
                $company->image = $image;
            }
        }
        $company->location=$_POST['location'];
        $company->address=$_POST['address'];
        $company->map=$_POST['map'];
        $company->contact_info=$_POST['contact_info'];
        $company->description=$_POST['description'];
        $company->email=$_POST['email'];
        $result = $company->updateCompanyById();
    }$records = $company->getCompanyById();
    require_once 'header.php';

?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Profile</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Edit Profile</h6>
                                </div>
                                <div class="card-body">
                                   <?php 
                                        if (isset($result) && $result == true) { ?>
                                        <div class="alert alert-success">Company update successful</div>
                                    <?php }elseif (isset($result) && $result == false) { ?>
                                        <div class="alert alert-danger">Company update failed</div>
                                    <?php } ?>
                                   <form id="editCompany" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $id ?>" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="name">Company Name:</label>
                                        <input type="text" name="name" id="name" value="<?php echo $records[0]->name ?>" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="size">Size:</label>
                                        <input type="text" name="size" id="size" value="<?php echo $records[0]->size ?>" class="form-control" required>
                                    </div>

                                   <div class="form-group">
                                        <label for="image">Company Logo:</label>
                                        <?php if(empty($records[0]->image)){?>
                                            <input type="file" accept="image/*" onchange="loadFile(event)" name="image" class="form-control" required>
                                            <img id="output" src="img/dummy.png" width="100px">
                                        <?php }else{ ?>
                                            <input type="file" accept="image/*" value="<?php $records[0]->image ?>" onchange="loadFile(event)" name="image" class="form-control">
                                            <img id="output" src="img/<?php echo $records[0]->image ?>" width="100px">
                                        <?php } ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" name="email" id="email" value="<?php echo $records[0]->email ?>" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="category">Location:</label>
                                        <select class="form-control" name="location">
                                            <option value="">--Select location--</option>
                                            <?php foreach($locations as $location){ ?>
                                                <option value="<?php echo $location->id ?>"<?php echo ($records[0]->location == $location->id)?"selected":'' ?>><?php echo $location->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Address:</label>
                                        <input type="text" name="address" id="address" value="<?php echo $records[0]->address ?>" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="map">Map:</label>
                                        <input type="text" name="map" id="map" value="<?php echo $records[0]->map ?>" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_info">Contact:</label>
                                        <input type="text" name="contact_info" id="contact_info" value="<?php echo $records[0]->contact_info ?>" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="description">About Company:</label>
                                        <textarea name="description" id="description" class="form-control" required><?php echo $records[0]->description ?></textarea> 
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="btnUpdate" value="Update" class="btn btn-success"> <a href="profile.php?id=<?php echo $_GET['id'] ?>" class="btn btn-info">Back</a>
                                    </div>
                                </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  
<!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- ckeditor-->
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
             jQuery.validator.addMethod("phoneNP", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, ""); 
    return this.optional(element) || phone_number.length >= 9 &&
        phone_number.match(/^(?:\+?([977]{3}))?[-. (]*([0]{1}[1]{1})[-. )]*(\d{3})[-. ]*(\d{4})$/);
}, "Please specify a valid phone number");
            $("#editCompany").validate({
                 ignore: [],
                 rules: {
                    contact_info: {
                       phoneNP: true
                    }, 
                }
                
            });
             Query.validator.addMethod("ckrequired", function (value, element) {  
            var idname = $(element).attr('id');  
            var editor = CKEDITOR.instances[idname];  
            var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();  
            if (ckValue.length === 0) {  
//if empty or trimmed value then remove extra spacing to current control  
                $(element).val(ckValue);  
            } else {  
//If not empty then leave the value as it is  
                $(element).val(editor.getData());  
            }  
            return $(element).val().length > 0;  
        }, "This field is required");  
  
function GetTextFromHtml(html) {  
            var dv = document.createElement("DIV");  
            dv.innerHTML = html;  
            return dv.textContent || dv.innerText || "";  
        }  
        });

        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
              var output = document.getElementById('output');
              output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        ClassicEditor
        .create( document.querySelector( '#description' ) )
        .catch( error => {
            console.error( error );
        } );
        
    </script>