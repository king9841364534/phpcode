<?php    
$company_name = $email = $password = $cpassword = ''; 

    if(isset($_POST['btnRegister'])){
        $error = [];

        if(isset($_POST['email']) && !empty($_POST['email']) && trim($_POST['email'])){
            $email = $_POST['email'];
            if(!filter_var($email , FILTER_VALIDATE_EMAIL)){
                $error['email'] = "Please enter a valid email";
            }
        }else{
            $error['email'] = "Please enter a valid email";
        }

        if(isset($_POST['company_name']) && !empty($_POST['company_name']) && trim($_POST['company_name'])){
            $company_name = $_POST['company_name'];
        }else{
            $error['company_name'] = "Please enter a valid Company Name";
        }

        if(isset($_POST['password']) && !empty($_POST['password'])){
            if(strlen($_POST['password']) > 4 ){
                $password = $_POST['password'];
            }else{
                $error['password'] = "Must have more than 4 word";
            }
        }else{
            $error['password'] = "Please provide a password";
        }

        if(isset($_POST['cpassword']) && !empty($_POST['cpassword'])){
           $cpassword = $_POST['cpassword'];
        }else{
            $error['cpassword'] = "Please provide a password";
        }

        if($password == $cpassword){   
        }else {
                $error['cpassword'] = "Password does not match";
        }

        if(count($error)==0){
            require_once '../admin/library/Company.php';
            $company = new Company();
            $company->name = $company_name;
            $company->email = $email;
            $company->password = md5($password);
            $result = $company->saveCompanyOfJobPortal();
            if($result == true){
                $error['ok'] = 'Account Created successfully';
            }else{
                $error['regfail'] =  'Registration failed';
            }
        }else{
                $error['regfail'] =  'Registration failed';
        }
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Company - Register</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <style type="text/css">
        .error {
            color: #ff5a02;
            font-size: 1rem;
            line-height: 1;
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        function checkAvailability() {
                $("#loaderIcon").show();
                jQuery.ajax({
                url:"check_availability.php",
                data:'email='+$("#exampleInputEmail").val(),
                type: "POST",
                success:function(data){
                $("#user-availability-status").html(data);
                $("#loaderIcon").hide();
                },
                error:function (){}
                });
            }
    </script>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                <?php if (isset($error['ok'])) { ?>
                                            <p class="alert alert-success"><?php  echo $error['ok']; ?></p>
                                          <?php } ?>

                                          <?php if (isset($error['regfail'])) { ?>
                                            <p class="alert alert-danger"><?php  echo $error['regfail']; ?></p>
                                          <?php } ?>
                            </div>
                            <form class="user" id="register" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
                                <div class="form-group">
                                    <input type="text" name="company_name" class="form-control form-control-user" id="exampleInputCompanyName"
                                        placeholder="Company Name" value="<?php echo $company_name ?>" autocomplete="off" >
                                        <span class="error">
                                                <?php echo (isset($error['company_name'])?$error['company_name']:'') ?>
                                            </span>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="email" value="<?php echo $email ?>" class="form-control form-control-user" id="exampleInputEmail"
                                        placeholder="Email Address" onBlur="checkAvailability()" autocomplete="off" >
                                        <span class="error" id="user-availability-status">
                                                <?php echo (isset($error['email'])?$error['email']:'') ?>
                                            </span>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" name="password" class="form-control form-control-user"
                                            id="exampleInputPassword" placeholder="Password" >
                                            <span class="error">
                                                <?php echo (isset($error['password'])?$error['password']:'') ?>
                                            </span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" name="cpassword" class="form-control form-control-user"
                                            id="exampleRepeatPassword" placeholder="Repeat Password" >
                                            <span class="error">
                                                <?php echo (isset($error['cpassword'])?$error['cpassword']:'') ?>
                                            </span>
                                    </div>
                                </div>
                                <input type="submit" name="btnRegister" id="btnRegister" value="Register Account" class="btn btn-primary btn-user btn-block">
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="index.php">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#register").validate({
                rules : {
                    password : {
                        required: true,
                        minlength : 5
                    },
                    cpassword : {
                        required: true,
                        minlength : 5,
                        equalTo : "#exampleInputPassword"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    company_name : "required",
                }
            });
        });

        
    </script>

</body>

</html>