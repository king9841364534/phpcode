<?php
    $title="List Applicants Company-JobPortal";
    require_once 'header.php';
    require_once '../admin/library/Applicant.php';
    $vacancy = new Applicant();
    $vacancy->set('company_id',$_GET['id']);
    $list = $vacancy->listApplicants();
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Applicants</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">List</h6>
                                </div>
                                <div class="card-body">
                                    <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                            <p class="alert alert-danger">Invalid Request</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                                            <p class="alert alert-success">Delete Success</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 3) { ?>
                                            <p class="alert alert-danger">Delete Failed</p>
                                          <?php } ?>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Applicant Name</th>
                                            <th>Email</th>
                                            <th>Vacancy</th>
                                            <th>Curriculum Vitae</th>
                                            <th>Cover letter</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>Applicant Name</th>
                                            <th>Email</th>
                                            <th>Vacancy</th>
                                            <th>Curriculum Vitae</th>
                                            <th>Cover letter</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($list as $in =>$query){ ?>
                                        <tr>
                                          <td><?php echo $in+1 ?></td>
                                            <td><?php echo $query->name ?></td>
                                            <td><?php echo $query->email ?></td>
                                            <td><?php echo $query->vacancy_name ?></td>
                                            <td><a href="uploads/<?php echo $query->cv_file ?>" target="_blank" class="btn btn-success"><i class="fa fa-download">Download</i></a></td>
                                            <td><a href="uploads/<?php echo $query->cl_file ?>" target="_blank" class="btn btn-success"><i class="fa fa-download">Download</i></a></td>
                                            <td><a href="delete_applicant.php?id=<?php echo $query->id ?>&cid=<?php echo $_GET['id'] ?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete')"><span class="fa fa-trash fw-fa"></span></a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  