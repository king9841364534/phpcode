<?php
    $title="Pofile Company-JobPortal";
    require_once 'header.php'; 
    $locations = $company->getLocation();
    $records = $company->getCompanyById();
    
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Profile</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                   <form>
                                    <center><img class="img-profile rounded-circle" src="img/<?php echo $records[0]->image ?>"></center>
                                    <div class="form-group">
                                        <label for="name">Company Name:</label>
                                        <input type="text" name="name" id="name" value="<?php echo $records[0]->name ?>" class="form-control" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label for="size">Size:</label>
                                        <input type="text" name="size" id="size" value="<?php echo $records[0]->size ?>" class="form-control" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" name="email" id="email" value="<?php echo $records[0]->email ?>" class="form-control" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label for="category">Location:</label>
                                        <select class="form-control" name="location" disabled>
                                            <option value="">--Select location--</option>
                                            <?php foreach($locations as $location){ ?>
                                                <option value="<?php echo $location->id ?>"<?php echo ($records[0]->location == $location->id)?"selected":'' ?>><?php echo $location->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Address:</label>
                                        <input type="text" name="address" id="address" value="<?php echo $records[0]->address ?>" class="form-control" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_info">Contact:</label>
                                        <input type="text" name="contact_info" id="contact_info" value="<?php echo $records[0]->contact_info ?>" class="form-control" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label for="description">About Company:</label>
                                        <p><?php echo $records[0]->description ?></p>
                                    </div>

                                    <div class="form-group">
                                        <label for="map">Map:</label>
                                        <div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $records[0]->map ?>"><a href="https://www.gps.ie/farm-gps/">tractor gps</a></iframe></div>
                                    </div>

                                    <div class="form-group">
                                        <a href="edit_profile.php?id=<?php echo $_GET['id'] ?>" class="btn btn-success">Edit</a>
                                    </div>
                                </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  
