<?php
$id = $_GET['id'];
$cid = $_GET['cid'];
if(!is_numeric($id)){
	header('location:list_applicants.php?msg=1&id='.$cid); 
}
require_once '../admin/library/Applicant.php';
$applicants = new Applicant();
$applicants->set('id',$id);
$records = $applicants->getApplicantById();
if(count($records) == 0){
	header('location:list_applicants.php?msg=1&id='.$cid); 
}
$msg = $applicants->deleteApplicantById();
if($msg == true) {
	header('location:list_applicants.php?msg=2&id='.$cid); 
}else{
	header('location:list_applicants.php?msg=3&id='.$cid);
}
?>