<?php
    $title="Change Password Company-JobPortal";
    require_once 'header.php';  
    $id = $_GET['id'];
    $company->set('id',$id);
    $result = $company->getCompanyById();
    if(isset($_POST['btnChangePassword'])){
        $error = '';
        if($result[0]->password == md5($_POST['password'])){
            $company->new_password = md5($_POST['new_password']);
            $result2 = $company->changeCompanyPassword();
        }else{
            $error ="Old password does not match";
        }
    }
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Change Password</h6>
                                </div>
                                <div class="card-body">
                                     <?php 
                                        if (isset($result2) && $result2 == true) { ?>
                                        <div class="alert alert-success">Password Change successful</div>
                                    <?php }elseif (isset($result) && $result == false) { ?>
                                        <div class="alert alert-danger">Password Change failed</div>
                                    <?php }elseif (isset($error)) { ?>
                                        <div class="alert alert-danger"><?php echo $error ?></div>
                                    <?php } ?> 
                                   <form id="changePassword" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $id ?>" method="POST">

                                    <div class="form-group">
                                        <label for="password">Old Password:</label>
                                        <input type="text" name="password" id="password" class="form-control" required autocomplete="off"><span id="same-password" style="font-size:20px;"></span>
                                    </div>

                                    <div class="form-group">
                                        <label for="new_password">New Password:</label>
                                        <input type="password" name="new_password" id="new_password" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="confirm_password">Confirm New Password:</label>
                                        <input type="password" name="confirm_password" class="form-control" required> 
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="btnChangePassword" value="Change" class="btn btn-success">
                                    </div>
                                </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

<!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#changePassword").validate({
                rules : {
                    new_password : {
                        required: true,
                        minlength : 5
                    },
                    confirm_password : {
                        required: true,
                        minlength : 5,
                        equalTo : "#new_password"
                    },
                    password : "required",
                }
            });
        });
    </script>