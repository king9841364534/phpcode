<?php
    $title="View Vacancy Company-JobPortal";
    require_once 'header.php'; 
    require_once '../admin/library/Vacancy.php';
    $id = $_GET['id'];
    $cid = $_GET['cid'];
	if(!is_numeric($id)){
		header('location:list_vacancy.php?msg=1&id='.$cid); 
	}
    
	$vacancy = new Vacancy();
	$vacancy->set('id',$cid);
    $records = $vacancy->listVacancyById();

?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Post</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">View / <a href="list_vacancy.php?id=<?php echo $id; ?>" class="btn btn-success">List</a></h6>
                                </div>

                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Category</th>
                                            <td><?php echo $records[0]->category ?></td>
                                        </tr>

                                        <tr>
                                            <th>Title</th>
                                            <td><?php echo $records[0]->title ?></td>
                                        </tr>

                                        <tr>
                                            <th>No of Employee Required</th>
                                            <td><?php echo $records[0]->require_no_of_employee ?></td>
                                        </tr>

                                        <tr>
                                            <th>Salaries</th>
                                            <td><?php if(empty($query->salaries)) {echo 'Negiotable';}else{echo $query->salaries; } ?></td>
                                        </tr>

                                        <tr>
                                            <th>Job Nature</th>
                                            <td><?php echo $records[0]->jobnature ?></td>
                                        </tr>

                                        <tr>
                                            <th>Experience</th>
                                            <td><?php echo $records[0]->expe ?></td>
                                        </tr>

                                        <tr>
                                            <th>Job Description</th>
                                            <td><?php echo $records[0]->job_description ?></td>
                                        </tr>

                                        <tr>
                                            <th>Job Requirement</th>
                                            <td><?php echo $records[0]->requirement ?></td>
                                        </tr>

                                        <tr>
                                            <th>Job Responsibilities</th>
                                            <td><?php echo $records[0]->responsibilities ?></td>
                                        </tr>

                                        <tr>
                                            <th>Published on</th>
                                            <td><?php echo $records[0]->published_on ?></td>
                                        </tr>

                                        <tr>
                                            <th>Date Line</th>
                                            <td><?php echo $records[0]->date_line ?></td>
                                        </tr>

                                    </table>

                                    <a href="list_vacancy.php?id=<?php echo $id ?>" class="btn btn-info"><--Back</a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

