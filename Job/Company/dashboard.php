<?php
    $title="Dashboard Company-JobPortal";
    require_once 'header.php';  

    require_once '../admin/library/Vacancy.php';
    $vacancy = new Vacancy(); 
    $vacancy->set('company_id',$_GET['id']); 
    $tvacancy = $vacancy->countVacancyByCompany();
    $avacancy = $vacancy->countActiveVacancyByCompany();
    $ivacancy = $vacancy->countInactiveVacancyByCompany();

    require_once '../admin/library/Applicant.php';
    $applicants = new Applicant(); 
    $applicants->set('company_id',$_GET['id']); 
    $tapplicant = $applicants->countApplicantsByCompany();

?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard</h1>

                      <div class="row">
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                Total Vacancy</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $tvacancy->totalvacancy ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Active Vacancy</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $avacancy->activevacancy ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-danger shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                Inactive or Expired Vacancy</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $ivacancy->inactivevacancy ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                Total Applicants</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $tapplicant->totalapplicants ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  