<?php
    $title="List Vacancy Company-JobPortal";
    require_once '../admin/library/Vacancy.php';
    $vacancy = new Vacancy();
    if(isset($_GET['cid']) && isset($_GET['active'])){
            $id=$_GET['cid'];
            $vacancy->set('id',$id);
            $vacancy->set('company_id',$_GET['id']);
            $vacancy->set('status',$_GET['active']);
            $vacancy->updateVacancyStatus();
    }
    require_once 'header.php';
    $vacancy->set('company_id',$_GET['id']);
    $list = $vacancy->listVacancy();
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Vacancy</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">List / <a href="create_vacancy.php?id=<?php echo $_GET['id']; ?>" class="btn btn-success">Create</a></h6>
                                </div>
                                <div class="card-body">
                                    <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                            <p class="alert alert-danger">Invalid Request</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                                            <p class="alert alert-success">Delete Success</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 3) { ?>
                                            <p class="alert alert-danger">Delete Failed</p>
                                          <?php } ?>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Category</th>
                                            <th>Title</th>
                                            <th>No of Employee Required</th>
                                            <th>Job Nature</th>
                                            <th>Salaries</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>Category</th>
                                            <th>Title</th>
                                            <th>No of Employee Required</th>
                                            <th>Job Nature</th>
                                            <th>Salaries</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($list as $in =>$query){ ?>
                                       <tr>
                                          <td><?php echo $in+1 ?></td>
                                            <td><?php echo $query->category ?></td>
                                            <td><?php echo $query->title ?></td>
                                            <td><?php echo $query->require_no_of_employee ?></td>
                                            <td><?php echo $query->job_nature ?></td>
                                            <td><?php if(empty($query->salaries)) {echo 'Negiotable';}else{echo $query->salaries; } ?></td>
                                             <td><?php if($query->status == 1 ){?>
                                                 <a class="btn btn-success" href="list_vacancy.php?cid=<?php echo $query->id; ?>.&active=1&id=<?php echo $_GET['id'] ?>" onclick="return confirm('Are you sure you want to block this admin?');" >Inactive</a>
                                            <?php } else {?>
                                                <a class="btn btn-danger" href="list_vacancy.php?cid=<?php echo $query->id; ?>.&active=0&id=<?php echo $_GET['id'] ?>" onclick="return confirm('Are you sure you want to active this admin?');">Active</a> 
                                            <?php } ?></td>
                                            <td><a href="view_vacancy.php?cid=<?php echo $query->id ?>&id=<?php echo $_GET['id'] ?>" class="btn btn-info"><img src="img/view.png" height="18" width="16"></a> <a href="edit_vacancy.php?cid=<?php echo $query->id ?>&id=<?php echo $_GET['id'] ?>" class="btn btn-warning"><span class="fa fa-edit fw-fa"></span></a> <a href="delete_vacancy.php?cid=<?php echo $query->id ?>&id=<?php echo $_GET['id'] ?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete')"><span class="fa fa-trash fw-fa"></span></a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  