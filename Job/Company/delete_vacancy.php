<?php
$id = $_GET['id'];
$cid = $_GET['cid'];
if(!is_numeric($id)){
	header('location:list_vacancy.php?msg=1&id='.$id); 
}
require_once '../admin/library/vacancy.php';
$vacancy = new Vacancy();
$vacancy->set('id',$cid);
$records = $vacancy->getVacancyById();
if(count($records) == 0){
	header('location:list_vacancy.php?msg=1&id='.$id); 
}
$msg = $vacancy->deleteVacancyById();
if($msg == true) {
	header('location:list_vacancy.php?msg=2&id='.$id); 
}else{
	header('location:list_vacancy.php?msg=3&id='.$id);
}
?>