<?php
    $title="Create Vacancy Company-JobPortal";

    require_once '../Admin/library/Vacancy.php';
    $vacancies = new Vacancy();

    if(isset($_POST['btnCreate'])){
        $vacancies->company_id = $_GET['id'];
        $vacancies->category_id = $_POST['category_id'];
        $vacancies->title = $_POST['title'];
        $vacancies->require_no_of_employee = $_POST['required_no_of_employee'];
        $vacancies->salaries = $_POST['salaries'];
        $vacancies->job_nature = $_POST['job_nature'];
        $vacancies->experience = $_POST['experience'];
        $vacancies->job_description = $_POST['job_description'];
        $vacancies->requirement = $_POST['requirement'];
        $vacancies->responsibilities = $_POST['responsibilities'];
        $vacancies->published_on = date('Y-m-d');
        $vacancies->date_line = $_POST['date_line'];
        $result = $vacancies->addJobVacancy();
    }
    require_once 'header.php';

    require_once '../Admin/library/Category.php';
    $category = new Category();
    $category_list = $category->displayCategoryOfJobPortal();

    require_once '../Admin/library/Job_Nature.php';
    $jobNature = new JobNature();
    $jobNature_list = $jobNature->displayNatureOfJob();

    require_once '../Admin/library/Experience.php';
    $jobExperience = new Experience();
    $jobExperience_list = $jobExperience->displayExperienceRequiredOfJob();
    
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Vacancy</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Create / <a href="list_vacancy.php?id=<?php echo $id; ?>" class="btn btn-success">List</a></h6>
                                </div>

                                <div class="card-body">
                                    <?php 
                                        if (isset($result) && $result == true) { ?>
                                        <div class="alert alert-success">Vacancy insert successful</div>
                                    <?php }elseif (isset($result) && $result == false) { ?>
                                        <div class="alert alert-danger">Vacancy insert failed</div>
                                    <?php } ?>
                                   <form id="createVacancy" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $_GET['id'] ?>" method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="category">Category:</label>
                                                <select class="form-control" name="category_id" required>
                                                    <option value="">--Select Job Category--</option>
                                                    <?php foreach($category_list as $category){ ?>
                                                        <option value="<?php echo $category->id ?>"><?php echo $category->title ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="job_nature">Job Nature:</label>
                                                <select class="form-control" name="job_nature" required>
                                                    <option value="">--Select Job Nature--</option>
                                                    <?php foreach($jobNature_list as $jobNature){ ?>
                                                        <option value="<?php echo $jobNature->id ?>"><?php echo $jobNature->title ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="experience">Experience:</label>
                                                <select class="form-control" name="experience" required>
                                                    <option value="">--Select Job Experience--</option>
                                                    <?php foreach($jobExperience_list as $jobExperience){ ?>
                                                        <option value="<?php echo $jobExperience->id ?>"><?php echo $jobExperience->title ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="title">Title:</label>
                                        <input type="text" name="title" id="title"  class="form-control" required>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="required_no_of_employee">Required No of Employee:</label>
                                                <input type="text"  name="required_no_of_employee" id="required_no_of_employee" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="salaries">Salaries:</label>
                                                <input type="text" name="salaries"  id="salaries"  class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="job_description">Job Description:</label>
                                        <textarea name="job_description" id="job_description" class="form-control" required></textarea><span class="error"></span>
                                    </div>

                                     <div class="form-group">
                                        <label for="requirement">Requirements:</label>
                                        <textarea name="requirement" id="requirement" class="form-control" required></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="responsibilities">Responsibilities:</label>
                                        <textarea name="responsibilities" id="responsibilities" class="form-control" required></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="date_line">Date Line:</label>
                                        <input type="date" min="<?php echo date('Y-m-d'); ?>" name="date_line" id="date_line" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="btnCreate" class="btn btn-success">
                                    </div>
                                </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

 <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>
 <!-- ckeditor-->
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>


    <script type="text/javascript">
        $(document).ready(function(){
            $("#createVacancy").validate({
               ignore: [], 
                rules: {
                    salaries: {
                      numbers: true
                    }, 
                    required_no_of_employee: {
                      required: true,
                      numbers: true
                    }
                },
                
            });
        

         jQuery.validator.addMethod("ckrequired", function (value, element) {  
            var idname = $(element).attr('id');  
            var editor = CKEDITOR.instances[idname];  
            var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();  
            if (ckValue.length === 0) {  
//if empty or trimmed value then remove extra spacing to current control  
                $(element).val(ckValue);  
            } else {  
//If not empty then leave the value as it is  
                $(element).val(editor.getData());  
            }  
            return $(element).val().length > 0;  
        }, "This field is required");  
  
        function GetTextFromHtml(html) {  
            var dv = document.createElement("DIV");  
            dv.innerHTML = html;  
            return dv.textContent || dv.innerText || "";  
        }  

        });

         ClassicEditor
        .create( document.querySelector( '#responsibilities' ) )
        .catch( error => {
            console.error( error );
        } );

        ClassicEditor
        .create( document.querySelector( '#requirement' ) )
        .catch( error => {
            console.error( error );
        } );

        ClassicEditor
        .create( document.querySelector( '#job_description' ) )
        .catch( error => {
            console.error( error );
        } );

    </script>