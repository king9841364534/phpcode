<?php require_once 'header.php'; 

$vacancies_list = $vacancies->listAllVacancy();
  if(!$vacancies_list){
    header('location:index.php');
  }
?>


        <!-- Header End -->
        <div class="container-xxl py-5 bg-dark page-header mb-5">
            <div class="container my-5 pt-5 pb-4">
                <h1 class="display-3 text-white mb-3 animated slideInDown">Jobs List</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb text-uppercase">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item text-white active" aria-current="page">All jobs</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- Header End -->


        <!-- Job Detail Start -->
        <div class="container-xxl py-5 wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row gy-5 gx-4">
                    <div class="col-lg">
                        <div class="tab-content">
                            <div class="job-item p-4 mb-4">
                                <div class="row g-4"><?php if($vacancies_list == null){?><h5 class="text-danger">No data found</h5><?php }else{ foreach ($vacancies_list as $vacancy) { ?> 
                                    <div class="col-sm-12 col-md-8 d-flex align-items-center">
                                        <?php if(!empty($vacancy->image)){?>
                                        <img class="flex-shrink-0 img-fluid border rounded" src="company/img/<?php echo $vacancy->image ?>" alt="" style="width: 80px; height: 80px;"><?php }else { ?>
                                        <img class="flex-shrink-0 img-fluid border rounded"  src="Company/img/dummy-company.png" alt="" style="width: 80px; height: 80px;">
                                        <?php } ?>
                                        
                                        <div class="text-start ps-4">
                                            <h5 class="mb-3"><?php echo $vacancy->title ?></h5>
                                            <span class="text-truncate me-3"><i class="fa fa-map-marker-alt text-primary me-2"></i><?php echo $vacancy->address ?></span>
                                            <span class="text-truncate me-3"><i class="far fa-clock text-primary me-2"></i><?php echo $vacancy->jobnature ?></span>
                                            <span class="text-truncate me-0"><i class="far fa-money-bill-alt text-primary me-2"></i><?php if(empty($vacancy->salaries)){ echo 'Negotiable'; }else{ echo 'Rs. '.$vacancy->salaries; }?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 d-flex flex-column align-items-start align-items-md-end justify-content-center">
                                        <div class="d-flex mb-3">
                                            <a class="btn btn-primary" href="job-detail.php?title=<?php echo $vacancy->title ?>&id=<?php echo $vacancy->id ?>">Apply Now</a>
                                        </div>
                                        <small class="text-truncate"><i class="far fa-calendar-alt text-primary me-2"></i>Date Line: <?php echo date("jS M Y", strtotime($vacancy->date_line)) ?></small>
                                    </div> <?php  } }?>
                                </div>
                            </div>
                        </div>
                    </div>
        
                    </div>
        
                </div>
            </div>
        </div>
        <!-- Job Detail End -->


<?php require_once 'footer.php' ?>