<?php
$id = $_GET['id'];
if(!is_numeric($id)){
	header('location:list_category.php?msg=1'); 
}
require_once 'library/Category.php';
$category = new Category();
$category->set('id',$id);
$records = $category->getCategoryById();
if(count($records) == 0){
	header('location:list_category.php?msg=1'); 
}
$msg = $category->deleteCategoryById();
if(!empty($records[0]->logo) && file_exists('img/' . $records[0]->logo)){
	unlink('img/' . $records[0]->logo);
}
if($msg == true) {
	header('location:list_category.php?msg=2'); 
}else{
	header('location:list_category.php?msg=3');
}
?>