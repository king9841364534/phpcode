<?php
$email = $password = ''; 

    if(isset($_POST['btnSubmit'])){
        $error = [];

        if(isset($_POST['email']) && !empty($_POST['email']) && trim($_POST['email'])){
            $email = $_POST['email'];
            if(!filter_var($email , FILTER_VALIDATE_EMAIL)){
                $error['email'] = "Please enter a valid email";
            }
        }else{
            $error['email'] = "Please enter a valid email";
        }

        if(isset($_POST['password']) && !empty($_POST['password'])){
            $password = $_POST['password'];
        }else{
            $error['password'] = "Please provide a password";
        }

        if(count($error)==0){
            require_once 'library/database.php';
            $db = new Database();
            $user = $db->checkAdminForLogin($email,md5($password)); 

            if($user){
                if($user->new_user == 0){
                session_start();
                $_SESSION['admin_name'] = $user->name;
                $_SESSION['admin_email'] = $user->emial;
                $_SESSION['admin_id'] = $user->id;
                header('location:dashboard.php');
            }elseif($user->new_user == 1){
                session_start();
                $_SESSION['admin_name'] = $user->name;
                $_SESSION['admin_email'] = $user->emial;
                $_SESSION['admin_id'] = $user->id;
                header('location:change_password.php');
            }
            }else{
                $error['loginfail'] =  'Login failed';
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>JobPortal Admin - Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">


    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <style type="text/css">
        .error {
            color: #ff5a02;
            font-size: 1rem;
            line-height: 1;
            width: 100%;
        }
    </style>

</head>

<body >
<section class="vh-100">
  <div class="container py-5 h-100">
    <div class="row d-flex align-items-center justify-content-center h-100">
      <div class="col-md-8 col-lg-7 col-xl-6">
        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
          class="img-fluid" alt="Phone image">
      </div>
      <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
        <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>

                                        <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                            <p class="alert alert-danger">Please login to continue</p>
                                          <?php } ?>

                                          <?php if (isset($error['loginfail'])) { ?>
                                            <p class="alert alert-danger"><?php  echo $error['loginfail']; ?></p>
                                          <?php } ?>
                                    </div>
        <form class="user" id="loginForm" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
          <!-- Email input -->
          <div class="form-outline mb-4">
            <label class="form-label" for="form1Example13">Email address</label>
            <input type="email" name="email" id="form1Example13" class="form-control form-control-lg" />
            <span class="error">
                <?php echo (isset($error['email'])?$error['email']:'') ?>
            </span>
          </div>

          <!-- Password input -->
          <div class="form-outline mb-4">
            <label class="form-label" for="form1Example23">Password</label>
            <input type="password" name="password" id="form1Example23" class="form-control form-control-lg" />
            <span class="error">
                <?php echo (isset($error['password'])?$error['password']:'') ?>
             </span>
          </div>

          <!-- Submit button -->
          <button type="submit" name="btnSubmit" class="btn btn-primary btn-lg btn-block">Sign in</button>

        </form>
      </div>
    </div>
  </div>
</section>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#loginForm").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 5
                    },
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    email: "Please enter a valid email address"
                }
            });
        });
    </script>
    </script>

</body>

</html>