<?php
$id = $_GET['id'];
$cid = $_GET['cid'];
if(!is_numeric($id)){
	header('location:vacancy_lists.php?msg=1'); 
}
require_once 'library/vacancy.php';
$vacancy = new Vacancy();
$vacancy->set('id',$id);
$records = $vacancy->getVacancyById();
if(count($records) == 0){
	header('location:vacancy_lists.php?msg=1'); 
}
$msg = $vacancy->deleteVacancyById();
if($msg == true) {
	header('location:vacancy_lists.php?msg=2'); 
}else{
	header('location:vacancy_lists.php?msg=3');
}
?>