<?php
$id = $_GET['id'];
if(!is_numeric($id)){
	header('location:company_lists.php?msg=1'); 
}
require_once '../admin/library/Company.php';
$company = new Company();
$company->set('id',$id);
$records = $company->getCompanyById();
if(count($records) == 0){
	header('location:company_lists.php?msg=1'); 
}
$msg = $company->deleteCompanyById();
if($msg == true) {
	header('location:company_lists.php?msg=2'); 
}else{
	header('location:company_lists.php?msg=3');
}
?>