<?php
    $title="List Category Admin-JobPortal";
    require_once 'header.php';
    require_once 'library/Category.php';
    $category = new Category();
    $list = $category->displayCategoryOfJobPortal();
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Category</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">List / <a href="create_category.php" class="btn btn-success">Create</a></h6>
                                </div>
                                <div class="card-body">
                                    <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                            <p class="alert alert-danger">Invalid Request</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                                            <p class="alert alert-success">Delete Success</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 3) { ?>
                                            <p class="alert alert-danger">Delete Failed</p>
                                          <?php } ?>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Created by</th>
                                            <th>Created at</th>
                                            <th>Updated by</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Created by</th>
                                            <th>Created at</th>
                                            <th>Updated by</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($list as $in =>$query){ ?>
                                        <tr>
                                            <td><?php echo $in+1 ?></td>
                                            <td><?php echo $query->title ?></td>
                                            <td><?php if(!empty($query->logo) && file_exists('img/' . $query->logo)){ ?>
                                                <img src="img/<?php echo $query->logo ?>" width='100px' >
                                            <?php }else { ?>
                                                <img src="img/dummy.png" width='150px'>
                                            <?php } ?></td>
                                            <td><?php echo $query->name ?> </td>
                                            <td><?php echo $query->created_at ?> </td>
                                            <td><?php if(empty($query->updated_by)){echo 'Null';}else{ echo $query->uname; } ?> </td>
                                            <td><?php if($query->updated_at == "0000-00-00 00:00:00"){echo 'Null';}else{ echo $query->updated_at; } ?> </td>
                                            <td><a href="edit_category.php?id=<?php echo $query->id ?>" class="btn btn-warning"><span class="fa fa-edit fw-fa"></a> <a href="delete_category.php?id=<?php echo $query->id ?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete')"><span class="fa fa-trash fw-fa"></a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  