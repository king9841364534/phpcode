<?php
    $title="Create Admin Admin-JobPortal";
    require_once 'header.php'; 
    if(isset($_POST['btnCreate'])){
        require_once 'library/database.php';
        $admin = new database();
            $admin->name = $_POST['name'];
            $admin->email = $_POST['email'];
            $admin->password = md5($_POST['password']);
            

            $result = $admin->saveAdminOfJobPortal();   
        
    }
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Admin</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Create / <a href="list_admin.php" class="btn btn-success">List</a></h6>
                                </div>

                                <div class="card-body">
                                    <?php 
                                        if (isset($result) && $result == true) { ?>
                                        <div class="alert alert-success">Admin added successful</div>
                                    <?php }elseif (isset($result) && $result == false) { ?>
                                        <div class="alert alert-danger">admin add failed</div>
                                    <?php } ?>
                                   <form id="createCategory" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <input type="text" name="name" id="name" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" name="email" id="email" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password:</label>
                                        <input type="text" name="password" value="admin1" id="password" class="form-control" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="submit" name="btnCreate" class="btn btn-success">
                                    </div>
                                </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

 <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#createCategory").validate({
                rules: {
                    image: {
                        required: true,
                        accept: "image/jpg,image/jpeg,image/png,image/gif",
                        filesize: 500000,
                    },
                    title: "required",
                    sub_category: "required",
                },
                messages: {
                    image: {
                        required: 'Please enter image', 
                        accept: 'Not an image!'
                    }
                }
            });
        });

        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
              var output = document.getElementById('output');
              output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

    </script>