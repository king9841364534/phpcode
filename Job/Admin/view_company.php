<?php
    $title="View Company Admin-JobPortal";
    require_once 'header.php'; 
    require_once '../admin/library/Company.php';
    $id = $_GET['id'];
	if(!is_numeric($id)){
		header('location:company_list.php?msg=1&id='.$id); 
	}
    
	$company = new Company();
	$company->set('id',$id);
    $records = $company->getCompanyById();

?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Post</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">View / <a href="vacancy_lists.php" class="btn btn-success">List</a></h6>
                                </div>

                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Company Name</th>
                                            <td><?php echo ucfirst($records[0]->name) ?></td>
                                        </tr>

                                        <tr>
                                            <th>Size</th>
                                            <td><?php echo $records[0]->size ?></td>
                                        </tr>

                                        <tr>
                                            <th>Image</th>
                                            <td><img src='../company/img/<?php echo $records[0]->image ?>' height="100px"></td>
                                        </tr>

                                        <tr>
                                            <th>Address</th>
                                            <td><?php echo $records[0]->address ?></td>
                                        </tr>

                                        

                                        <tr>
                                            <th>Phone</th>
                                            <td><?php echo $records[0]->contact_info ?></td>
                                        </tr>

                                        <tr>
                                            <th>Email</th>
                                            <td><?php echo $records[0]->email ?></td>
                                        </tr>

                                        <tr>
                                            <th>Description</th>
                                            <td><?php echo $records[0]->description ?></td>
                                        </tr>

                                        <tr>
                                            <th>Map</th>
                                            <td><div style="width: 100%"><iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $records[0]->map ?>"><a href="https://www.gps.ie/farm-gps/">tractor gps</a></iframe></div></td>
                                        </tr>

                                       
                                    </table>

                                    <a href="company_lists.php" class="btn btn-info"><--Back</a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

