<?php 
require_once 'database.php';

class Vacancy extends database{
	public $id,$c_id,$company_id,$category_id,$title,$require_no_of_employee,$salaries,$job_nature,$experience,$job_description,$requirement,$responsibilities,$published_on,$date_line,$status;

	function addJobVacancy(){
		$sql = "insert into vacancies(company_id,category_id,title,require_no_of_employee,salaries,job_nature,experience,job_description,requirement,responsibilities,published_on,date_line) values('$this->company_id','$this->category_id','$this->title','$this->require_no_of_employee','$this->salaries','$this->job_nature','$this->experience','$this->job_description','$this->requirement','$this->responsibilities','$this->published_on','$this->date_line')";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1 && $this->connection->insert_id > 0){
			return true;
		}else{
			return false;
		}
	}

	function countVacancy(){
		$sql= "select count(*) as totalvacancy from vacancies";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countActiveVacancy(){
		$sql= "select count(*) as activevacancy from vacancies where status=1 and date_line >= CURDATE();";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countInactiveVacancy(){
		$sql= "select count(*) as inactivevacancy from vacancies where status=0 or date_line < CURDATE();";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countVacancyByCompany(){
		$sql= "select count(*) as totalvacancy from vacancies where company_id='$this->company_id'";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countActiveVacancyByCompany(){
		$sql= "select count(*) as activevacancy from vacancies where status=1 and company_id='$this->company_id' and date_line >= CURDATE();";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countInactiveVacancyByCompany(){
		$sql= "select count(*) as inactivevacancy from vacancies where status=0 and company_id='$this->company_id' or date_line < CURDATE();";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function listVacancy(){
		$sql = "select v.*,c.title as category,e.title as experience,j.title as jobnature from vacancies v join categories c on v.category_id=c.id join experiences e on v.experience=e.id join job_nature j on v.job_nature=j.id join companies o on o.id=v.company_id where v.company_id = '$this->company_id' and o.status=1";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function displayAllVacancy(){
		$sql = "select v.*,c.title as category,e.title as experience,j.title as jobnature from vacancies v join categories c on v.category_id=c.id join experiences e on v.experience=e.id join job_nature j on v.job_nature=j.id join companies o on o.id=v.company_id where o.status=1 order by v.id desc";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function listVacancyByCategory(){
		$sql = "select v.* from vacancies v join companies c on v.company_id=c.id where v.category_id = '$this->c_id' and c.status=1 and v.status=1 and v.date_line >= CURDATE();";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function getVacancyById(){
		$sql = "select v.*,c.name,c.image,c.map,c.address as addres,l.name as address,c.description,j.title as jobnature from vacancies v join companies c on v.company_id=c.id join locations l on c.location=l.id join job_nature j on v.job_nature=j.id where v.id = '$this->id' and c.status=1";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function deleteVacancyById(){
		$sql = "delete from vacancies where id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function listVacancyById(){
		$sql = "select v.*,c.title as category,e.title as expe,o.name as company,j.title as jobnature from vacancies v join categories c on v.category_id=c.id join experiences e on v.experience=e.id join job_nature j on v.job_nature=j.id join companies o on o.id=v.company_id where v.id = '$this->id' and o.status=1";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function updateVacancyById(){
		$sql = "update vacancies set category_id='$this->category_id',title='$this->title',require_no_of_employee='$this->require_no_of_employee',salaries='$this->salaries',job_nature='$this->job_nature',experience='$this->experience',job_description='$this->job_description',requirement='$this->requirement',responsibilities='$this->responsibilities',published_on='$this->published_on',date_line='$this->date_line' where id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function listVacancyByNature(){
		if(isset($this->category_id)){
		$sql = "select v.*,c.title as category,e.title as expe,o.image,j.title as jobnature,o.location,l.name as address from vacancies v join categories c on v.category_id=c.id join experiences e on v.experience=e.id join job_nature j on v.job_nature=j.id join companies o on v.company_id=o.id join locations l on l.id=o.location where v.job_nature = '$this->job_nature' and o.status=1 and v.category_id='$this->category_id' and v.status=1 and v.date_line >= CURDATE(); ";
		}else{
			$sql = "select v.*,c.title as category,e.title as expe,o.image,j.title as jobnature,o.location,l.name as address from vacancies v join categories c on v.category_id=c.id join experiences e on v.experience=e.id join job_nature j on v.job_nature=j.id join companies o on v.company_id=o.id join locations l on l.id=o.location where v.job_nature = '$this->job_nature'  and o.status=1 and v.status=1 and v.date_line >= CURDATE();";
	}
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}return $data; 
		}
	}

	function listVacancyByCompany(){
			$sql = "select v.*,c.title as category,e.title as expe,o.image,j.title as jobnature,o.location,l.name as address from vacancies v join categories c on v.category_id=c.id join experiences e on v.experience=e.id join job_nature j on v.job_nature=j.id join companies o on v.company_id=o.id join locations l on l.id=o.location where v.status=1 and o.status=1 and v.company_id='$this->company_id' and v.date_line >= CURDATE();";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}return $data; 
		}
	}

	function listAllVacancy(){
			$sql = "select v.*,c.title as category,e.title as expe,o.image,j.title as jobnature,o.location,l.name as address from vacancies v join categories c on v.category_id=c.id join experiences e on v.experience=e.id join job_nature j on v.job_nature=j.id join companies o on v.company_id=o.id join locations l on l.id=o.location where v.status=1 and o.status=1 and v.date_line >= CURDATE();";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}return $data; 
		}
	}

	function updateVacancyStatus(){
		if($this->status == 1){
			$sql = "update vacancies set status='0' where id=$this->id";
		}else if($this->status == 0){
			$sql = "update vacancies set status='1' where id=$this->id";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

}
?>