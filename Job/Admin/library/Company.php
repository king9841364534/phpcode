<?php
date_default_timezone_set('Asia/Kathmandu');
require_once 'database.php';
class Company extends database{
	public $new_password,$name,$size,$image,$location,$address,$map,$contact_info,$description,$email,$password;

	function saveCompanyOfJobPortal(){
		$sql = "insert into companies(name,email,password,new_user) values ('$this->name','$this->email','$this->password','1')";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1 && $this->connection->insert_id > 0){
			return true;
		}else{
			return false;
		}
	}

	function countCompany(){
		$sql= "select count(*) as totalcompany from companies";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countActiveCompany(){
		$sql= "select count(*) as activecompany from companies where status=1";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countInactiveCompany(){
		$sql= "select count(*) as inactivecompany from companies where status=0";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}			

	function displayCompanyOfJobPortal(){
		$sql= "SELECT * FROM companies";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			} 
		}return $data; 
	}

	function checkCompanyForLogin($email,$password){
		$sql = "select * from companies where email='$email' and password='$password' and status=1";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object();
		} 
	}

	function checkEmailForRegistration($email){
		$sql = "select * from companies where email='$email'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function getCompanyById(){
		$sql = "select * from companies where id='$this->id'";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows == 1){
			$data[] = $result->fetch_object();
		}return $data; 
	}

	function changeCompanyPassword(){
		$sql = "update companies set password='$this->new_password' where id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function getLocation(){
		$sql = "select * from locations order by name asc";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function updateCompanyById(){
		if(empty($this->image)){
		$sql = "update companies set name='$this->name',size='$this->size',location='$this->location',address='$this->address',map='$this->map',contact_info='$this->contact_info',description='$this->description',email='$this->email',new_user='0' where id='$this->id'";
		}else{
			$sql = "update companies set name='$this->name',size='$this->size',image='$this->image',location='$this->location',address='$this->address',map='$this->map',contact_info='$this->contact_info',description='$this->description',email='$this->email',new_user='0' where id='$this->id'";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function updateCompanyStatus(){
		if($this->status == 1){
			$sql = "update companies set status='0' where id=$this->id";
		}else if($this->status == 0){
			$sql = "update companies set status='1' where id=$this->id";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function deleteCompanyById(){
		$sql = "delete from vacancies where company_id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			$sql = "delete from companies where id='$this->id'";
			$this->connectDB();
			$this->connection->query($sql);
			if($this->connection->affected_rows == 1){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function set($key,$value){
		$this->$key = $value ;
	}

	function get($key){
		return $this->$key ;
	}

}

?>