<?php 
require_once 'database.php';

class Category extends database{
	public $id,$title,$category_id,$sub_category,$image,$created_by,$created_at,$updated_by,$updated_at;

	function saveCategoryOfJobPortal(){
		if(empty($this->image)){
			$sql = "insert into categories(title,created_by,created_at) values ('$this->title','$this->created_by','$this->created_at')";
		}else{
			$sql = "insert into categories(title,logo,created_by,created_at) values ('$this->title','$this->image','$this->created_by','$this->created_at')";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1 && $this->connection->insert_id > 0){
			return true;
		}else{
			return false;
		}
	}

	function countCategory(){
		$sql= "select count(*) as totalcategory from categories";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}

	function displayCategoryOfJobPortal(){
		$sql= "select c.*,a.name,u.name as uname from categories c join admins a on c.created_by=a.id left join admins u on c.updated_by=u.id order by id asc";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function getCategoryById(){
		$sql= "select c.*,a.name,u.name as uname from categories c join admins a on c.created_by=a.id left join admins u on c.updated_by=u.id where c.id=$this->id";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows == 1){
			$data[] = $result->fetch_object();
		}return $data; 
	}

	function deleteCategoryById(){
		$sql = "delete from categories where id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function updateCategoryOfJobPortal(){
		if(empty($this->image)){
			$sql = "update categories set title='$this->title',updated_by='$this->updated_by',updated_at='$this->updated_at' where id='$this->id'";
		}else{
			$sql = "update categories set title='$this->title',logo='$this->image',updated_by='$this->updated_by',updated_at='$this->updated_at' where id='$this->id'";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

}

?>