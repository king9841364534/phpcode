<?php 
require_once 'database.php';

class Applicant extends database{
	public $id,$vacancy_id,$name,$email,$cl_file,$cv_file,$company_id;

	function saveApplicantDetails(){
		$sql = "insert into applicants(vacancy_id,name,email,cl_file,cv_file) values ('$this->vacancy_id','$this->name','$this->email','$this->cl_file','$this->cv_file')";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1 && $this->connection->insert_id > 0){
			echo "<script>alert('Your applicant is sent')</script>";
		}else{
			return false;
		}
	}

	function countApplicantsByCompany(){
		$sql= "select count(*) as totalapplicants from applicants a join vacancies v on a.vacancy_id=v.id where v.company_id='$this->company_id'";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function listApplicants(){
		$sql = "select a.*,v.title as vacancy_name from applicants a join vacancies v on a.vacancy_id=v.id where v.company_id='$this->company_id' order by a.id desc";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function getApplicantById(){
		$sql = "select * from applicants where id = '$this->id'";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function deleteApplicantById(){
		$sql = "delete from applicants where id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

}
?>