<?php
date_default_timezone_set('Asia/Kathmandu');
class Database{
	public $connection,$new_password;

	function connectDB(){
		try{
			$this->connection = new mysqli('localhost','root','','job_portal');
			return $this->connection;
		}catch(Exception $e){
			die('Database Error ' . $e->getMessage());
		}
	}

	function saveAdminOfJobPortal(){
		$sql = "insert into admins(name,email,password,status,new_user) values ('$this->name','$this->email','$this->password','1','1')";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1 && $this->connection->insert_id > 0){
			return true;
		}else{
			return false;
		}
	}

	function displayAdminOfJobPortal(){
		$sql= "select * from admins where id<>$this->id";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function countAdmin(){
		$sql= "select count(*) as totaladmin from admins";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countActiveAdmin(){
		$sql= "select count(*) as activeadmin from admins where status=1";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}	

	function countInactiveAdmin(){
		$sql= "select count(*) as inactiveadmin from admins where status=0";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object(); 
		}
	}			

	function updateAdminStatus(){
		if($this->status == 1){
			$sql = "update admins set status='0' where id=$this->id";
		}else if($this->status == 0){
			$sql = "update admins set status='1' where id=$this->id";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function checkAdminForLogin($email,$password){
		$sql = "select * from admins where email='$email' and password='$password' and status=1";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object();
		} 
	}

	function getAdminById(){
		$sql = "select * from admins where id='$this->id'";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows == 1){
			return $result->fetch_object();
		} 
	}

	function changeAdminPassword(){
		$sql = "update admins set password='$this->new_password',new_user='0' where id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function set($key,$value){
		$this->$key = $value ;
	}

	function get($key){
		return $this->$key ;
	}

}

?>