<?php 
require_once 'database.php';

class Experience extends database{

	function displayExperienceRequiredOfJob(){
		$sql= "select * from experiences order by id asc";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

}
?>