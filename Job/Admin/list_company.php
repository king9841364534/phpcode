<?php
    $title="List Admin Admin-JobPortal";
    require_once 'library/Database.php';
    $database = new Database();
   
    if(isset($_GET['id']) && isset($_GET['active'])){
            $id=$_GET['id'];
            $database->set('id',$id);
            $database->set('status',$_GET['active']);
            $database->updateAdminStatus();
    }
    require_once 'header.php';
     $inid =$_SESSION['admin_id']; 
    $database->set('id',$inid);
    $list = $database->displayAdminOfJobPortal();
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Admin</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">List / <a href="create_admin.php" class="btn btn-success">Create</a></h6>
                                </div>
                                <div class="card-body">
                                    <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                            <p class="alert alert-danger">Invalid Request</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                                            <p class="alert alert-success">Delete Success</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 3) { ?>
                                            <p class="alert alert-danger">Delete Failed</p>
                                          <?php } ?>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Photo</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>Photo</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($list as $in =>$query){ ?>
                                        <tr>
                                            <td><?php echo $in+1 ?></td>
                                            <td><?php if(!empty($query->image) && file_exists('img/' . $query->image)){ ?>
                                                <img src="img/<?php echo $query->image ?>" width='100px' >
                                            <?php }else { ?>
                                                <img src="img/dummy2.png" width='100px'>
                                            <?php } ?></td>
                                            <td><?php echo $query->name ?></td>
                                            <td><?php echo $query->email ?></td>
                                            <td><?php if($query->status == 1 ){?>
                                                 <a class="btn btn-success" href="list_admin.php?id=<?php echo $query->id; ?>.&active=1" onclick="return confirm('Are you sure you want to block this admin?');" >Inactive</a>
                                            <?php } else {?>
                                                <a class="btn btn-danger" href="list_admin.php?id=<?php echo $query->id; ?>.&active=0" onclick="return confirm('Are you sure you want to active this admin?');">Active</a> 
                                            <?php } ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  