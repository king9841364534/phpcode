<?php
    $title="Dashboard Admin-JobPortal";
    require_once 'header.php';  
    require_once 'library/database.php';
    $admin = new database();  
    $tadmin = $admin->countAdmin();
    $aadmin = $admin->countActiveAdmin();
    $iadmin = $admin->countInactiveAdmin();

    require_once 'library/Category.php';
    $category = new Category();  
    $tcategory = $category->countCategory();

    require_once 'library/Company.php';
    $company = new Company();  
    $tcompany = $company->countCompany();
    $acompany = $company->countActiveCompany();
    $icompany = $company->countInactiveCompany();

    require_once 'library/Vacancy.php';
    $vacancy = new Vacancy();  
    $tvacancy = $vacancy->countVacancy();
    $avacancy = $vacancy->countActiveVacancy();
    $ivacancy = $vacancy->countInactiveVacancy();

?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard</h1>

                     <div class="row">
                         <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                Total Admins</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $tadmin->totaladmin ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Active Admins</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $aadmin->activeadmin ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-danger shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                Inactive Admins</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $iadmin->inactiveadmin ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                Category</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $tcategory->totalcategory ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                Total Company</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $tcompany->totalcompany ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Active Company</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $acompany->activecompany ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-danger shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                Inactive Company</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $icompany->inactivecompany ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                Total Vacancy</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $tvacancy->totalvacancy ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Active Vacancy</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $avacancy->activevacancy ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-danger shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                Inactive Vacancy</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $ivacancy->inactivevacancy ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  