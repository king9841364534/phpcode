<?php require_once 'header.php';
if(isset($_GET['id'])){
$vacancies->set('category_id',$_GET['id']);
} 
?>


        <!-- Header End -->
        <div class="container-xxl py-5 bg-dark page-header mb-5">
            <div class="container my-5 pt-5 pb-4">
                <h1 class="display-3 text-white mb-3 animated slideInDown">Job List</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb text-uppercase">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item text-white active" aria-current="page">Job List</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- Header End -->


        <!-- Jobs Start -->
        <div class="container-xxl py-5">
            <div class="container">
                <h1 class="text-center mb-5 wow fadeInUp" data-wow-delay="0.1s">Job Listing</h1>
                <div class="tab-class text-center wow fadeInUp" data-wow-delay="0.3s">
                    <ul class="nav nav-pills d-inline-flex justify-content-center border-bottom mb-5">
                        <?php foreach($jobNature_list as $in =>$jobNature) { ?>
                        <li class="nav-item"><?php if($in == 0){ ?>
                            <a class="d-flex align-items-center text-start mx-3 me-0 pb-3 active" data-bs-toggle="pill" href="#tab-<?php echo $in+1 ?>"> <?php }else { ?>
                                <a class="d-flex align-items-center text-start mx-3 me-0 pb-3" data-bs-toggle="pill" href="#tab-<?php echo $in+1 ?>">
                            <?php } ?>
                                <h6 class="mt-n1 mb-0"><?php echo $jobNature->title ?></h6>
                            </a>
                        </li>
                        <?php } ?> 
                    </ul>
                    <div class="tab-content">
                        <?php foreach ($jobNature_list as $in =>$jobNature) { ?><?php if($in == 0){ ?>
                        <div id="tab-<?php echo $jobNature->id; ?>" class="tab-pane fade show p-0 active"> <?php }else { ?><div id="tab-<?php echo $jobNature->id; ?>" class="tab-pane fade show p-0 "> <?php } ?>
                            <div class="job-item p-4 mb-4">
                                <div class="row g-4"><?php $vacancies->set('job_nature',$jobNature->id); $vacancies_list = $vacancies->listVacancyByNature(); if($vacancies_list == null){?><h5 class="text-danger">No data found</h5><?php }else{ foreach ($vacancies_list as $vacancy) { ?>
                                    <div class="col-sm-12 col-md-8 d-flex align-items-center">
                                        <?php if(!empty($vacancy->image)){?>
                                        <img class="flex-shrink-0 img-fluid border rounded" src="company/img/<?php echo $vacancy->image ?>" alt="" style="width: 80px; height: 80px;"><?php }else { ?>
                                        <img class="flex-shrink-0 img-fluid border rounded"  src="Company/img/dummy-company.png" alt="" style="width: 80px; height: 80px;">
                                        <?php } ?>
                                        <div class="text-start ps-4">
                                            <h5 class="mb-3"><?php echo $vacancy->title ?></h5>
                                            <span class="text-truncate me-3"><i class="fa fa-map-marker-alt text-primary me-2"></i><?php echo $vacancy->address ?></span>
                                            <span class="text-truncate me-3"><i class="far fa-clock text-primary me-2"></i><?php echo $vacancy->jobnature ?></span>
                                            <span class="text-truncate me-0"><i class="far fa-money-bill-alt text-primary me-2"></i><?php if(empty($vacancy->salaries)){ echo 'Negotiable'; }else{ echo 'Rs. '.$vacancy->salaries; }?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 d-flex flex-column align-items-start align-items-md-end justify-content-center">
                                        <div class="d-flex mb-3">
                                            <a class="btn btn-primary" href="job-detail.php?title=<?php echo $vacancy->title ?>&id=<?php echo $vacancy->id ?>">Apply Now</a>
                                        </div>
                                        <small class="text-truncate"><i class="far fa-calendar-alt text-primary me-2"></i>Date Line: <?php echo date("jS M Y", strtotime($vacancy->date_line)) ?></small>
                                    </div> <?php } }?>
                                </div>
                            </div>
                            <a class="btn btn-primary py-3 px-5" href="all_jobs.php">All Jobs</a>
                        </div>
                     <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Jobs End -->


<?php require_once 'footer.php' ?>