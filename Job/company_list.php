<?php require_once 'header.php' ?>

        <!-- Header End -->
        <div class="container-xxl py-5 bg-dark page-header mb-5">
            <div class="container my-5 pt-5 pb-4">
                <h1 class="display-3 text-white mb-3 animated slideInDown">Company</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb text-uppercase">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item text-white active" aria-current="page">Company</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- Header End -->


        <!-- Company Start -->
        <div class="container-xxl py-5">
            <div class="container">
                <h1 class="text-center mb-5 wow fadeInUp" data-wow-delay="0.1s">Companies asscoiated with us</h1>
                <div class="row g-4">
                    <?php if($company_list == null){?><h5 class="text-danger">No data found</h5><?php }else{ foreach ($company_list as $company) { ?>
                    <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.1s">
                            <a class="cat-item rounded p-4" href="company_detail.php?id=<?php echo $company->id ?>"><?php if(!empty($company->image)){?>
                            <img src="Company/img/<?php echo $company->image ?>" height="100"><?php }else { ?>
                            <img src="Company/img/dummy-company.png" height="100">
                            <?php } ?>
                            <h6 class="mb-3"><?php echo $company->name ?></h6>
                            <p class="mb-0"><?php $vacancies->set('company_id',$company->id); if(!empty($vacancies->listVacancyByCompany())){ echo count($vacancies->listVacancyByCompany());}else{ echo '0';} ?> Vacancy</p>
                        </a>
                    </div>
                    <?php } }?>
                </div>
            </div>
        </div>
        <!-- Company End -->


<?php require_once 'footer.php' ?>