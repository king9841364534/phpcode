<?php require_once 'header.php'; 
$company->set('id',$_GET['id']);
$vacancies->set('company_id',$_GET['id']);
$vacancies_list = $vacancies->listVacancyByCompany();
$companies_details = $company->getCompanyById();
  if(!$companies_details){
    header('location:index.php');
  }
?>


        <!-- Header End -->
        <div class="container-xxl py-5 bg-dark page-header mb-5">
            <div class="container my-5 pt-5 pb-4">
                <h1 class="display-3 text-white mb-3 animated slideInDown">Company Detail</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb text-uppercase">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item text-white active" aria-current="page">Company Detail</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- Header End -->


        <!-- Job Detail Start -->
        <div class="container-xxl py-5 wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row gy-5 gx-4">
                    <div class="col-lg">
                        <div class="d-flex align-items-center mb-5">
                            <?php if(!empty($company->image)){?>
                            <img class="flex-shrink-0 img-fluid border rounded" src="company/img/<?php echo $companies_details[0]->image ?>" alt="" style="width: 80px; height: 80px;"><?php }else { ?>
                            <img class="flex-shrink-0 img-fluid border rounded"  src="Company/img/dummy-company.png" alt="" style="width: 80px; height: 80px;">
                            <?php } ?>
                            <div class="text-start ps-4">
                                <h3 class="mb-3"><?php echo $companies_details[0]->name ?></h3>
                                <span class="text-truncate me-3"><i class="fa fa-map-marker-alt text-primary me-2"></i><?php echo $companies_details[0]->address ?></span>
                                <span class="text-truncate me-3"><i class="far fa-envelope text-primary me-2"></i><?php echo $companies_details[0]->email ?></span>
                            </div>
                        </div>

                        <div class="mb-5">
                            <h4 class="mb-3">Can you describe your company in a few sentences?</h4>
                            <p><?php echo $companies_details[0]->description ?></p>
                            <div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $companies_details[0]->map ?>"><a href="https://www.gps.ie/farm-gps/">tractor gps</a></iframe></div>
                        </div>

                        <div class="tab-content"><h5>Active Jobs</h5>
                            <div class="job-item p-4 mb-4">
                                <div class="row g-4"><?php if($vacancies_list == null){?><h5 class="text-danger">No data found</h5><?php }else{ foreach ($vacancies_list as $vacancy) { ?> 
                                    <div class="col-sm-12 col-md-8 d-flex align-items-center">
                                        <?php if(!empty($vacancy->image)){?>
                                        <img class="flex-shrink-0 img-fluid border rounded" src="company/img/<?php echo $vacancy->image ?>" alt="" style="width: 80px; height: 80px;"><?php }else { ?>
                                        <img class="flex-shrink-0 img-fluid border rounded"  src="Company/img/dummy-company.png" alt="" style="width: 80px; height: 80px;">
                                        <?php } ?>
                                        
                                        <div class="text-start ps-4">
                                            <h5 class="mb-3"><?php echo $vacancy->title ?></h5>
                                            <span class="text-truncate me-3"><i class="fa fa-map-marker-alt text-primary me-2"></i><?php echo $vacancy->address ?></span>
                                            <span class="text-truncate me-3"><i class="far fa-clock text-primary me-2"></i><?php echo $vacancy->jobnature ?></span>
                                            <span class="text-truncate me-0"><i class="far fa-money-bill-alt text-primary me-2"></i><?php if(empty($vacancy->salaries)){ echo 'Negotiable'; }else{ echo 'Rs. '.$vacancy->salaries; }?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 d-flex flex-column align-items-start align-items-md-end justify-content-center">
                                        <div class="d-flex mb-3">
                                            <a class="btn btn-primary" href="job-detail.php?title=<?php echo $vacancy->title ?>&id=<?php echo $vacancy->id ?>">Apply Now</a>
                                        </div>
                                        <small class="text-truncate"><i class="far fa-calendar-alt text-primary me-2"></i>Date Line: <?php echo date("jS M Y", strtotime($vacancy->date_line)) ?></small>
                                    </div> <?php  } }?>
                                </div>
                            </div>
                        </div>
                    </div>
        
                    </div>
        
                </div>
            </div>
        </div>
        <!-- Job Detail End -->


<?php require_once 'footer.php' ?>