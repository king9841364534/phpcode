<?php
  require_once 'constant.php';
  if(isset($_POST['signup'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];
    $password = md5($_POST['password']);
    $address = $_POST['address'];
    try{
        $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
        $sql = "insert into users(name , email , phone , password , address) values ('$name', '$email', '$mobile' , '$password' , '$address')";
        if($connection->query($sql)){
          $msg = 'Registration Successful. Your account will be active within 24hr';
        }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    } 
  }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	<title>
		Register Page
	</title>
</head>
<body>
  <?php require_once 'menu.php'; ?>
  <div class="container mt-5">
    <div class="row d-flex align-items-center justify-content-center">
        <div class="col-md-6">
            <div class="card px-5 py-5">
              <form action= "<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form" > 
                <h5 class="mt-3 mb-5"><center>Create Account</center></h5>
                <?php 
                  if (isset($msg)) { ?>
                     <p class="alert alert-success"><?php  echo $msg; ?></p>
                  <?php } ?>
                <div class="form-input"> <i class="fa fa-user"></i> <input type="text" name="name" class="form-control" placeholder="Enter Name" pattern="^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}$" title="Name must have at least 4 character" required autocomplete="off"> </div>
                <div class="form-input"> <i class="fa fa-phone"></i> <input type="text" name="mobile" class="form-control" placeholder="Enter Mobile Number" pattern="[9]{1}[7-8]{1}[0-9]{8}" title="Mobile number with 98 or 97 and remaing 8 digit with 0-9" required autocomplete="off"> </div>
                <div class="form-input"> <i class="fa fa-location-pin"></i> <input type="text" name="address" class="form-control" placeholder="Enter Address" pattern="^[a-zA-Z\s]+$"title="Address must contain only letter" required autocomplete="off"> </div>
                <div class="form-input"> <i class="fa fa-envelope"></i> <input type="text" name="email" class="form-control" pattern="[a-z0-9]+@[a-z]+\.[a-z]{2,3}" title="should have a @ symbol, as well as some string preceeding it, and some string proceeding, the second string needs to contain a dot, which has an additional 2-3 characters after that." placeholder="Enter Email address" required autocomplete="off"> </div>
                <div class="form-input"> <i class="fa fa-lock"></i> <input type="password" name="password" class="form-control" placeholder="Enter Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$" title="Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character" required autocomplete="off"> </div>
                <input type="submit" value="Register now" name="signup" class="mt-4 signup">
                <div class="text-center mt-4"> <span>Already a member?</span> <a href="login.php" class="text-decoration-none">Login</a> </div>
              </form>
            </div>
        </div>
    </div>
  </div>
</body>
</html>