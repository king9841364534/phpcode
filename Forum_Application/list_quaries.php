<?php 
  require_once 'constant.php';
  date_default_timezone_set('Asia/Kathmandu');
  require_once 'function.php';

    try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to insert data
      $sql = "select f.*,c.title as category_title,u.name from forums as f join categories as c on f.category_id=c.id join users as u on f.posted_by=u.id order by f.posted_date desc";
      //exceute query and get result object
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($data, $row);
        }
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }     
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	<title>
		List Quaries Page
	</title>
</head>
<body>
    <?php require_once 'menu.php'; ?>
    <div class="container-fluid">
     <h2 class="text-center">List queries</h2>
        <div class="row">
            <div class="col-md-12">
              <div class="album py-5">
    <div class="container">

      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3">
        <?php foreach($data as $query){ ?>
        <div class="col">
          <div class="card shadow-sm">
            <?php 
            if (!empty($query->image) && file_exists('images/' . $query->image)) { ?>
              <img src="images/<?php echo $query->image ?>" height="246px">
          <?php  } else { ?>
              <img src="images/dummy-post.png" height="246px">
          <?php  } ?>

            <div class="card-body">
              <p class="card-text"><?php echo $query->title ?><br>
                <span class="badge rounded-pill bg-primary"><?php echo $query->category_title ?></span>
                <span class="badge rounded-pill bg-success">Posted By:<?php echo $query->name ?></span>

              </p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-primary"><a href="view.php?id=<?php echo $query->id ?>">View</a></button>
                 
                </div>
                <small class="text-muted">
                  <?php 
                     echo getPostedTime($query->posted_date);
                   ?>
                </small>
              </div>
            </div>
          </div>
        </div>
       <?php } ?>
      </div>
    </div>
  </div>
            </div>
        </div>  
    </div>
</body>
</html>