<?php 
session_start();
require_once '../constant.php';
require_once '../check_session.php';
try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
     $sql = "select forums.id,forums.category_id,forums.title,forums.description,forums.image,forums.status,forums.posted_by,forums.posted_date,categories.title as category_title,users.name as user_name from forums inner join categories on forums.category_id=categories.id inner join users on forums.posted_by=users.id where forums.status=1 and forums.posted_by=".$_SESSION['userid']." order by forums.posted_date desc ";
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($data, $row);
        }
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	<title>
		Dashboard Page
	</title>
</head>
<body>
    <?php require_once '../menu.php'; ?>
    <div class="container mt-5">
                <div class="card px-5 py-5">
                    <img class="user" src="https://i.ibb.co/yVGxFPR/2.png" height="100px" width="100px"> 
                    <h5 class="mt-3 mb-5"><center><?php echo $_SESSION['name'] ?> Dashboard</center></h5>
                     <div class="card">
                        <div class="card-header bg-info">
                          Questions
                        </div>
                        <div class="card-body">
                  <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                   <p class="alert alert-success">Category Deleted Successfully</p>
                 <?php } ?>
                 <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                   <p class="alert alert-success">Category Delete Failed</p>
                 <?php } ?>
                   <table class="table table-bordered table-dark table-striped">
                     <thead>
                       <tr>
                         <th>SN</th>
                         <th>Category</th>
                         <th>Title</th>
                         <th>Image</th>
                         <th>Description</th>
                         <th>Posted Date</th>
                         <th>Posted By</th>
                         <th>Action</th>
                       </tr>
                     </thead>
                     <tbody>
                      <?php foreach($data as $in => $record){ ?>
                       <tr>
                        <td><?php echo $in+1  ?></td>
                        <td><?php echo $record->category_title ?></td>
                        <td><?php echo $record->title ?></td>
                        <td><?php if(!empty($record->image)){?>
                            <img class="rounded img-fluid" src="../images/<?php echo $record->image; ?>">
                    <?php } else{ ?>
                            <input class="form-control border-danger text-danger text-center" type="text" value="NONE" disabled>
                    <?php }?></td>
                        <td><?php echo $record->description ?></td>
                        <td><?php echo $record->posted_date ?></td>
                        <td><?php echo $record->user_name ?></td>
                         <td>
                          <a href="edit_question.php?id=<?php echo $record->id ?>" class="btn btn-warning">Edit</a>
                          <a href="delete_question.php?id=<?php echo $record->id ?>" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>
                         </td>
                       </tr>
                     <?php } ?>
                     </tbody>
                   </table>
                </div>
                </div>
            
            </div>
        
    </div>
</body>
</html>