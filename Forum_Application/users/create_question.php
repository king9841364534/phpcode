<?php 
session_start();
require_once '../constant.php';
require_once '../check_session.php';
date_default_timezone_set('Asia/Kathmandu');
$category = $title = $description = $image =  '';
$status = 0;
$connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      $sql = "select * from categories";
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($data, $row);
        }
      }
if (isset($_POST['btnSave'])) {
    $cid=$_POST['category']; 
    $title = $_POST['title'];
    $description = $_POST['description'];
    $posted_by=$_SESSION['userid'];
    $date = date('y/m/d H:i:s');
    print_r($_FILES);
     if(isset($_FILES['image'])){
            if ($_FILES['image']['error'] == 0) {
          if ($_FILES['image']['size'] <= 50000000) {
            $types = ['image/jpeg','image/gif','image/png'];
              if (in_array($_FILES['image']['type'], $types)) {
                
                $image = uniqid() . '_' .$_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'], '../images/' . $image);
                $sql = "insert into forums(category_id,title,description,image,posted_date,posted_by) values ($cid,'$title','$description','$image','$date','$posted_by')";
                
            }
          } 
      }
        }
       else{
            $sql = "insert into forums(category_id,title,description,posted_date,posted_by) values ($cid,'$title','$description','$date','$posted_by')";
        }
      if($connection->query($sql)){
        $msg =  'Question added successfully. your question will be active within 24hr';
        
      }
    } 
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	<title>
		Create Question Page
	</title>
</head>
<body>
    <?php require_once '../menu.php'; ?>
    <div class="container mt-5">
                <div class="card px-5 py-5">
                    <div class="card">
                        <div class="card-header bg-info">
                          Question Create
                        </div>
                        <div class="card-body">
                            <?php 
                                if (isset($msg)) { ?>
                            <p class="alert alert-success"><?php  echo $msg; ?></p>
                            <?php } ?>
                            <form accept="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                         <?php require_once 'question_mainpage.php'; ?>
                        
                              <div class="form-group mt-2">
                                <input type="submit" value="Save" name="btnSave" class="btn btn-success">
                            </div>
                            </form>
                        </div>
                </div>
        </div>
    </div>
</body>
</html>