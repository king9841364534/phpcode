
                                <div class="form-group">
                                <label for="title">Category</label>
                                <select name="category" class="form-control">
                                    <option value="">Please Select category</option><?php
                                        foreach ($data as $record) { ?>
                                         <option value="<?php echo $record->id ?>"<?php echo ($record->id == $category) ?'selected':'' ?>><?php echo $record->title ?></option>
                                        <?php } ?>
                                </select>
                                </div>
                                <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" value="<?php echo $title ?>" required autocomplete="off">
                              </div>
                              <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name = "description" class="form-control" required><?php echo $description ?></textarea>
                              </div>
                              <div>
                                <label for="image">Image</label><br>
                                <input type="file" name="image" value="<?php echo $image; ?>" > 
                              </div>