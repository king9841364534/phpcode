<?php 
session_start();
require_once '../constant.php';
require_once '../check_session.php';
$title = $description = $image =  '';
$id = $_GET['id'];
  if (!is_numeric($_GET['id'])) {
    die('Invalid data');
  }
$connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      $sql = "select * from categories";
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          array_push($data, $row);
        }
      }
      

if (isset($_POST['btnUpdate'])) {
    $cid=$_POST['category']; 
    $title = $_POST['title'];
    $description = $_POST['description'];
    $image = $_POST['image'];
    $posted_by=$_SESSION['userid'];
    $date = date('y/m/d');
    try{
        if(!empty($image)){
            $sql = "update forums set category_id = $cid , title = '$title', description = '$description' , image = '$image' where id=$id";
        }else{
            $sql = "update forums set category_id = $cid , title = '$title', description = '$description' where id=$id";
        }
      if($connection->query($sql)){
        $msg =  'Question updated successfully';
      }
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }
}

try{
     $sql = "select * from forums where id=$id";
     $result = $connection->query($sql);
     if($result->num_rows == 1){
        $record = $result->fetch_object();
        $category = $record->category_id;
        $title = $record->title;
        $description = $record->description;
        $image = $record->image;
     } else {
      $msgerror =  'Question not found';
     }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <title>
        Create Question Page
    </title>
</head>
<body>
    <?php require_once '../menu.php'; ?>
    <div class="container mt-5">
                <div class="card px-5 py-5">
                    <div class="card">
                        <div class="card-header bg-info">
                          Question Create
                        </div>
                        <div class="card-body">
                            <?php 
                                if (isset($msg)) { ?>
                            <p class="alert alert-success"><?php  echo $msg; ?></p>
                            <?php } ?>
                            <form accept="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                         <?php require_once 'question_mainpage.php'; ?>
                        
                              <div class="form-group mt-2">
                                <input type="submit" value="Update" name="btnUpdate" class="btn btn-success">
                            </div>
                            </form>
                        </div>
                </div>
        </div>
    </div>
</body>
</html>