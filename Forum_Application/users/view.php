<?php 
session_start();
require_once '../constant.php';
require_once '../check_session.php';
date_default_timezone_set('Asia/Kathmandu');
require_once '../function.php';
$id = $_GET['id'];
  if (!is_numeric($_GET['id'])) {
    die('Invalid data');
  }
    try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to insert data
      $sql = "select f.*,c.title as category_title,u.name from forums as f join categories as c on f.category_id=c.id join users as u on f.posted_by=u.id where f.id=$id";
     $result = $connection->query($sql);
      if ($result->num_rows > 0) {
        $row = $result->fetch_object();
      } else {
        header('location:list_queries.php');
      }
      $connection->close();
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    } 


if (isset($_POST['reply'])) {
    $reply = $_POST['reply'];
    $forum_id = $_GET['id'];
    $reply_by =  $_SESSION['userid'];
    $reply_date =  date('Y-m-d H:i:s');
    
  try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
     $sql = "insert into forum_replies(forum_id,reply_by,reply,reply_date) values ('$forum_id','$reply_by','$reply','$reply_date')";
      if($connection->query($sql)){
        $msg =  'Replied  successfully';
        header("location:view.php?id=$id#reply");
      } $connection->close();
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }
}

 ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <title>
        List Quaries Page
    </title>
</head>
<body>
    <?php require_once '../menu.php'; ?>
    <div class="container-fluid ">
 <h2 class="text-center">Forum Details</h2>
        <div class="row">
            <div class="col-md-12">
              <div class="album py-5 bg-info">
    <div class="container">

    <div class="col-md-12">
      

      <article class="blog-post">
        <h2 class="blog-post-title"><?php echo $row->title ?></h2>
        <p class="blog-post-meta"><?php echo $row->posted_date ?> by <a href="#"><?php echo $row->name ?></a></p>

       
        <hr>
        <p style="text-align: justify;">
          <?php echo $row->description; ?>


        </p>
        <div class="mx-auto">
           <?php 
            if (!empty($query->image) && file_exists('images/' . $query->image)) { ?>
             <img  class="mx-auto" width="100%" src="../images/<?php echo $row->image ?>">
          <?php  } ?>
           
        </div><br>
        <hr>
        <h3 id="reply">Replies</h3>   
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $row->id ?>" method="post">
          <div class="mb-3">
            <textarea name="reply" class="form-control" id="exampleFormControlTextarea1"  placeholder="write your response here"></textarea>
          </div>
          <div class="mb-3">
            <button type="submit" name="btnPost" class="btn btn-success">Post</button>
          </div>
          </form><br>
          <div class="card">
            <div class="card-body">
          <h2>Comment/s</h2>
          <hr class="divider" style="max-width: 100%">
          <div><?php
            try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
     $sql = "select fr.*,u.name,u.email,f.title as title from forum_replies as fr join users as u on fr.reply_by=u.id join forums as f on fr.forum_id=f.id where fr.forum_id=$id";
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          
          array_push($data, $row);
        }
      } $connection->close();
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }
    foreach($data as $query){  
    ?>
      <div class="d-flex justify-content-between align-items-center">
      <div class="btn-group mb-3">
                 <p class="mb-0"><large><b><?php echo $query->name ?></b></large>  <span class="text-primary"><small class="mb-0"><i><?php echo $query->email ?></i></small></span></p>
                </div>
                <small class="text-muted">
                  <?php 
                     echo getPostedTime($query->reply_date);
                   ?>
                </small>    
            </div>
            <p style="text-align: justify;">
          <?php echo $query->reply; ?>
        </p>   
        <a href="like.php?frid=<?php echo $query->id?>&like=<?php echo $query->no_of_like?>&fid=<?php echo $query->forum_id?>"><button class="btn btn-primary"><?php echo $query->no_of_like ?> <i class="fa fa-thumbs-up" aria-hidden="true"></i></button></a> <a href="dislike.php?frid=<?php echo $query->id?>&dislike=<?php echo $query->dislike?>&fid=<?php echo $query->forum_id?>"><button class="btn btn-primary"><?php echo $query->dislike ?> <i class="fa fa-thumbs-down" aria-hidden="true"></i></button></a>
        <hr>
        <?php } ?>
          <div>
          </div>
    </div>
    </div>
  </div>
            </div>
        </div>
    </div>

</body>
</html>