<?php 
  require_once 'constant.php';
  if(isset($_POST['login'])) {
    $email=$_POST['email'];
    $password=md5($_POST['password']);
      try{
          $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
          $sql = "select * from admins where email='$email' and password='$password'";
          $result = $connection->query($sql);
          if ($result->num_rows == 1) {
            while ($row = $result->fetch_object()) {
                session_start();
                $_SESSION['adminid']=$email;
                $_SESSION['name']=$row->name;
                header("Location:admin/dashbord.php");
            }
        }else{
            $msg = "Name or Password not correct";
        }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
      }
  }
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	<title>
		Admin Login Page
	</title>
</head>
<body>
    <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-md-6">
                <div class="card px-5 py-5">
                    <img class="user" src="https://i.ibb.co/yVGxFPR/2.png" height="100px" width="100px"> 
                    <h5 class="mt-3 mb-3"><center>Admin Login</center></h5>
                    <?php 
                        if (isset($msg)) { ?>
                            <p class="alert alert-danger"><?php  echo $msg; ?></p>s
                    <?php } ?>
                    <form action= "<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form" >
                        <div class="form-input"> <i class="fa fa-envelope"></i> <input type="email" name="email" class="form-control" placeholder="Email address" required autocomplete="off"> </div>
                        <div class="form-input"> <i class="fa fa-lock"></i> <input type="password" name="password" class="form-control" placeholder="password" required> </div>
                        <input type="submit" value="login" name="login" class="mt-4 signup">
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>