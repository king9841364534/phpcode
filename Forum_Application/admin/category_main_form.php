<div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" value="<?php echo $title ?>" required autocomplete="off">
              </div>
              <div class="form-group">
                <label for="code">Code</label>
                <input type="text" name="code" class="form-control" value="<?php echo $code ?>" required autocomplete="off">
              </div>
              <div class="form-group">
                <label for="rank">Rank</label>
                <input type="text" name="rank" class="form-control" value="<?php echo $rank ?>" pattern="\d+" min="0" title="Enter numeric value only" required autocomplete="off">
              </div>
              <div class="form-group">
                <label for="status">Status</label>
                <input type="radio" name="status" value="1" <?php echo ($status == 1) ?'checked':'' ?>>Active
                <input type="radio" name="status" value="0" <?php echo ($status == 0) ?'checked':'' ?> >De-Active
              </div>