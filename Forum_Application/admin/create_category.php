<?php 
session_start();
require_once '../constant.php';
require_once '../check_session2.php';
$title = $code = $rank =  '';
$status = 0;
if (isset($_POST['btnSave'])) {
    $title = $_POST['title'];
    $code = $_POST['code'];
    $rank = $_POST['rank'];
    $status = $_POST['status'];
    try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      $sql = "insert into categories(title,code,rank,status) values ('$title','$code',$rank,$status)";
      if($connection->query($sql)){
        $msg =  'Category added successfully';
        $title = $code = $rank =  '';
        $status = 0;
      }
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	<title>
		Create Category Page
	</title>
</head>
<body>
    <?php require_once '../menu.php'; ?>
    <div class="container mt-5">
                <div class="card px-5 py-5">
                    <div class="card">
                        <div class="card-header bg-info">
                          Category Create
                        </div>
                        <div class="card-body">
                            <?php 
                                if (isset($msg)) { ?>
                            <p class="alert alert-success"><?php  echo $msg; ?></p>
                            <?php } ?>
                            <form accept="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                            <?php require_once 'category_main_form.php' ?>
                            <div class="form-group mt-2">
                                <input type="submit" value="Save" name="btnSave" class="btn btn-success">
                            </div>
                            </form>
                        </div>
                </div>
        </div>
    </div>
</body>
</html>