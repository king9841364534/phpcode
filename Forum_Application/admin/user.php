<?php 
session_start();
require_once '../constant.php';
require_once '../check_session2.php';
try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      $sql = "select * from users";
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($data, $row);
        }
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }

if(isset($_GET['inid'])){
      $id=$_GET['inid'];
      $status=1;
      $sql = "update users set Status='$status'  WHERE id='$id'";
      $result = $connection->query($sql);
      if($result){
        header('location:user.php');
      }
    }

    if(isset($_GET['id'])){
      $id=$_GET['id'];
      $status=0;
      $sql = "update users set Status='$status'  WHERE id='$id'";
      $result = $connection->query($sql);
      if($result){
        header('location:user.php');
      }
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	<title>
		List User Page
	</title>
</head>
<body>
    <?php require_once '../menu.php'; ?>
    <div class="container mt-5">
                <div class="card px-5 py-5">
                    <div class="card">
                        <div class="card-header bg-info">
                          User List
                        </div>
                        <div class="card-body">
                  <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                   <p class="alert alert-success">User Deleted Successfully</p>
                 <?php } ?>
                 <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                   <p class="alert alert-success">User Delete Failed</p>
                 <?php } ?>
                   <table class="table table-bordered table-dark table-striped">
                     <thead>
                       <tr>
                         <th>SN</th>
                         <th>Name</th>
                         <th>Phone</th>
                         <th>Address</th>
                         <th>Email</th>
                         <th>Status</th>
                         <th>Action</th>
                       </tr>
                     </thead>
                     <tbody>
                      <?php foreach($data as $in => $record){ ?>
                       <tr>
                        <td><?php echo $in+1  ?></td>
                         <td><?php echo $record->name ?></td>
                         <td><?php echo $record->phone ?></td>
                         <td><?php echo $record->address ?></td>
                         <td><?php echo $record->email ?></td>
                         <td><?php if ($record->status == 0) { ?>
                          <a href="user.php?inid=<?php echo $record->id; ?>" onclick="return confirm('Are you sure you want to active this user?');" >  <button class="btn btn-danger"> Inactive</button></a>
                       <?php } else {?>
                            <a href="user.php?id=<?php echo $record->id; ?>" onclick="return confirm('Are you sure you want to block this user?');"><button class="btn btn-success"> Active</button></a>
                        <?php } ?></td>
                         <td>
                          <a href="delete_user.php?id=<?php echo $record->id ?>" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>
                         </td>
                       </tr>
                     <?php } ?>
                     </tbody>
                   </table>
                </div>
                </div>
        </div>
    </div>
</body>
</html>