<?php 
function getPostedTime($posted_date){
  $datetime1 = new DateTime($posted_date);
  $datetime2 = new DateTime(date('Y-m-d H:i:s'));
  $difference = $datetime1->diff($datetime2);
  $msg  =  'Posted:';
  if ($difference->y >0) {
    $msg .= $difference->y.' years';
  }
  if ($difference->m >0) {
    $msg .= $difference->m.' months';
  }
  if ($difference->d >0) {
    $msg .= $difference->d.' days';
  }
  if ($difference->h >0) {
    $msg .= $difference->h.' hours';
  }

  if ($difference->i >0) {
    $msg .= $difference->i.' minutes';
  }
  if ($difference->i <= 0) {
    $msg .= 'just now';
  }
  if ($difference->i > 0) {
   $msg .= ' ago.';
  }
  return $msg;
}


 ?>