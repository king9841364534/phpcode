<?php 
  require_once 'constant.php';
  date_default_timezone_set('Asia/Kathmandu');
  require_once 'function.php';
  $id = $_GET['id'];
  if (!is_numeric($_GET['id'])) {
    die('Invalid data');
  }
    try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to insert data
      $sql = "select f.*,c.title as category_title,u.name from forums as f join categories as c on f.category_id=c.id join users as u on f.posted_by=u.id where f.id=$id";
      //exceute query and get result object
      $result = $connection->query($sql);
      if ($result->num_rows > 0) {
        $query = $result->fetch_object();
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }     
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
  <title>
    List Quaries Page
  </title>
</head>
<body>
    <?php require_once 'menu.php'; ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
              <div class="album py-5">
    <div class="container">

      
        <div class="col">
          <div class="card shadow-sm">
             <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                   <h2 class="text-success"><i><?php echo $query->title ?></i></h2><br>
                </div>
                <small>
                 <span class="float-right mr-4 text-primary"><i>Posted By: <?php echo ucwords($query->name) ?></i></span>
                 <span class="float-right mr-4"><i><?php echo $query->posted_date ?></i></span>
               </small>
              </div>
          
              
            </p>
            
            <?php 
            if (!empty($query->image) && file_exists('images/' . $query->image)) { ?>
              <img src="images/<?php echo $query->image ?>">
          <?php  } else { ?>
              <img src="images/dummy-post.png">
          <?php  } ?>

            <div class="card-body">
              <p class="card-text">
                <span class="badge rounded-pill bg-primary"><?php echo $query->category_title ?></span>
               

              </p>
              <p class="card-text">
                <?php echo $query->description; ?>
              </p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-primary"><a href="list_quaries.php">Back</a></button>
                 
                </div>
                <small class="text-muted">
                  <?php 
                     echo getPostedTime($query->posted_date);
                   ?>
                </small>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
            </div>
        </div>  
    
</body>
</html>