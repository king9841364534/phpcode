<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
  <title>	Home Page
	</title>
</head>
<body>
    <?php require_once 'menu.php'; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5">
              <div class="card bg-secondary">
                <div class="card-header bg-info">
                  Index Page
                </div>
                <div class="card-body">
                	<a href="register.php" class="btn btn-success btn-lg">Register</a>
                	<a href="login.php" class="btn btn-primary btn-lg">Login</a>
                </div>
                <div class="card-footer">
        		Developed by: Milan Karki  
                </div>
            </div>
        </div>
    </div>
</body>
</html>