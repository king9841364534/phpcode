<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<?php
	$name = array( "Ram" => "31" , "Hari" => "41" , "Sita" => "39" , "Gita" => "40");
	print_r($name);
	echo "<br>"; 
	echo "Ascending order key<br>";
	ksort($name); 
	print_r($name);
	echo "<br>"; 
	echo "Ascending order value<br>";
	asort($name); 
	print_r($name);
	echo "<br>";
	echo "Descending order key<br>"; 
	krsort($name); 
	print_r($name);
	echo "<br>"; 
	echo "Descending order value<br>";
	arsort($name); 
	print_r($name);
?>
</body>
</html>