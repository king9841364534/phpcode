<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<?php
	$brand = array('A' => 'Apple', 'B' => 'Bird', 'c' => 'Carbon');
	echo "Lower case : <br>";
	foreach ($brand as $value) {
	  	echo strtolower($value) . '<br>';
	}
	echo "Upper case : <br>";
	foreach ($brand as $value) {
	  	echo strtoupper($value) . '<br>';
	}   
?>
</body>
</html>