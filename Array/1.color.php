<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<?php
		$color = array('white' , 'green' , 'red' , 'blue' , 'black' , 'sky');
		echo "The memory of that scene for me is like a frame of film forever frozen at that moment: the $color[2] carpet, the $color[1] lawn, the $color[0] house, the leaden $color[5]. The new president and his first lady. - Richard M. Nixon";
	?>
</body>
</html>