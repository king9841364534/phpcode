<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		
	</title>
</head>
<body>
<?php 
	$list = array( "Nepal" => "Kathmandu" , "India" => "New Delhi" , "Australia" => "Sydney", "Denmark" => "Copenhagen" , "Finland" => "Helsinki" , "France" => "Paris", "Slovakia" => "Bratislava" , "Netherlands" => "Amsterdam" , "Portugal" => "Lisbon", "Spain" => "Madrid") ;
	foreach ($list as $key => $value) {
 		echo 'The capital of ' . ucfirst($key) . ' is ' . ucfirst($value) . '<br>'; 
 	}
?>
</body>
</html>