<?php 
$Username = '';
if(isset($_POST['login'])){
	if(isset($_POST['username']) && !empty($_POST['username']) && trim($_POST['username'])){
		$Username =  $_POST['username'];
	}else{
		$errUsername =  'Enter username';
	}
	if(isset($_POST['password']) && !empty($_POST['password'])){
		$Password = $_POST['password'];
	}else{
		$errPassword = 'Enter password';
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<!--
		1. Define 'action' and 'method' attribute inside form opening tag
			Action : Location to send form data into website(Where), defult is same page 
			Method : Process to send data into website(How), defult is GET
				- GET 
				- POST
				- PUT/PATCH
				- DELETE
		2. Name attribute inside form element : Which can be accessed as associative index array
		3. Access form data using following array
			$_GET : if method is POST
			$_POST : if method is GET
	 -->
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
	<label>Username:</label>
	<input type="text" name="username" value="<?php echo $Username; ?>">
	<?php if(isset($errUsername)){ echo $errUsername; } ?>
	<br>
	<label>Password:</label>
	<input type="password" name="password">
	<?php if(isset($errPassword)){ echo $errPassword; } ?>
	<br>
	<input type="submit" name="login">
</form>
</body>
</html>