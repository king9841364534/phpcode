<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<?php
//foreach always in array.

$info = [20, 'Ram' , 'KTM' , 60.5 , 'ram@gmail.com'];
for ($i=0; $i < count($info) ; $i++) { 
 	echo $i + 1 . ')' . $info[$i] . '<br>';
 } 

 $info1 = ['s.n' => 20, 'roll' => 12 , 'name' => 'Ram' , 'age' => 20 , 'weight' => 56.5 , 'email' => 'ram@gmail.com'];
 echo '<ul type = "circle" >'; //unordered list
 foreach ($info1 as $key => $value) {
 	echo '<li>' . ucfirst($key) . ' is ' . $value . '<br>'; //ucfirst 1 letter of word will be capital
 }
 echo '</ul>';

 $info2 = ['s.n' => 20, 'roll' => 12 , 'name' => 'Ram' , 'age' => 20 , 'weight' => 56.5 , 'email' => 'ram@gmail.com'];
 ?>
 <table border='1' width="30%">
 <?php foreach ($info2 as $key => $value) { ?>
 	
		<tr>
			<th><?php echo ucfirst($key) ?></th>
			<td><?php echo $value ?></td>
		</tr>
 <?php } 
 
?>
 </table>
</body>
</html>