<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
	</title>
</head>
<body>
<?php
/*
if(condition){
	//statement if condition is true
} else{
	//statement if condition is false
}
*/
$name = 12;
if(is_string($name)){
	echo 'My name is ' . $name;
}

$name = 'Ram';
if(is_string($name)){
	echo '<br>My name is ' . $name;
}

$num = 123;
if(is_integer($num)){
	echo '<br>Number is ' . $num;
}else{
	echo '<br>' . $num . ' is not a integer';
}

unset($num);
$num = 123.332;
if(is_integer($num)){
	echo '<br>Number is ' . $num;
}else{
	echo '<br>' . $num . ' is not a integer';
}

unset($num);
$num = 123.554;
if(is_float($num)){
	echo '<br>Float is ' . $num;
}else{
	echo '<br>' . $num . ' is not a float number';
}

unset($num);
$num = 123;
if(is_float($num)){
	echo '<br>Float is ' . $num;
}else{
	echo '<br>' . $num . ' is not a float number';
}

$a = 1;
if(is_bool($a)){
	echo '<br>a is ' . $a;
}else{
	echo '<br>' . $a . ' is not boolen';
}

unset($a);
$a = true;
if(is_bool($a)){
	echo '<br>a is ' . $a;
}else{
	echo '<br>' . $a . ' is not boolen';
} 

unset($a);
$a = false;
if(is_bool($a)){
	echo '<br>a is ' . $a;
}else{
	echo '<br>' . $a . ' is not boolen';
} 

?>
</body>
</html>