<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<button>Hide</button><button>Show</button><button>Toggle</button><button>Red</button><button>Slide_Up</button><button>Slide_Down</button><button>Slide_Toggle</button><button>Animate</button>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	//$(selector).method();
	$(document).ready(function(){
		$('button').click(function(){
			if($(this).text() == "Hide"){
				$('p').hide(5000);
			}if($(this).text() == "Show"){
				$('p').show(5000);
			}if($(this).text() == "Toggle"){
				$('p').toggle();
			}if($(this).text() == "Red"){
				$('p').css({'color':'red','border':'1px solid green','padding':'200px'});
			}if($(this).text() == "Slide_Up"){
				$('p').slideUp(5000);
			}if($(this).text() == "Slide_Down"){
				$('p').slideDown(5000);
			}if($(this).text() == "Slide_Toggle"){
				$('p').slideToggle();
			}if($(this).text() == "Animate"){
				$('p').animate({padding:'300px'});
			}
		})
	});
</script>
</body>
</html>