<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style type="text/css">
		label{
			margin-bottom: 10px;
			width: 15%;
			display: inline-block;
		}span{
			color: red;
		}
	</style>
</head>
<body>
<h1> Register </h1>
<form action="" method="post" onsubmit="return validateForm()">
	<label for="name">Name</label>
	<input type="text" name="name" id="name">
	<span id="err_name" class="error"></span>
	<br>
	<label for="email">Email</label>
	<input type="text" name="email" id="email">
	<span id="err_email" class="error"></span>
	<br>
	<label for="phone">Phone</label>
	<input type="text" name="phone" id="phone">
	<span id="err_phone" class="error"></span>
	<br>
	<label for="dob">Date of Birth</label>
	<input type="text" name="dob" id="dob">
	<span id="err_dob" class="error"></span>
	<br>
	<label for="country">Country</label>
	<select name="country" id="country">
		<option value="">--Select Country--</option>
		<option value="nepal">Nepal</option>
		<option value="india">India</option>
		<option value="china">China</option>
		<option value="japan">Japan</option>
	</select>
	<span id="err_country" class="error"></span>
	<br>
	<label for="password">Password</label>
	<input type="text" name="password" id="password">
	<span id="err_password" class="error"></span>
	<br>
	<input type="submit" name="Register">
</form>
<script type="text/javascript" src="js/form.js"></script>
</body>
</html>