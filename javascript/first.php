<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>	
<!-- inline -->
<button onclick="alert('hello')">Click Me</button>
<noscript>Please enable Javascript to run your program</noscript>
<!-- internal -->
<script type="text/javascript">
	alert("Milan");
</script>
<!-- external -->
<script type="text/javascript" src="js/my.js"></script>
</body>
</html>