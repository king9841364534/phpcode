//print in window
/*document.write("hi");
//print in console
console.log("This is log test");
console.info("This is log test");
console.warn("This is log test");
console.error("This is log test");
console.dir("This is log test"); */

var a = 20;
console.log(a);
var a = 30;
console.log(a);

let b = 20;
console.log(b);
b = 30;
console.log(b);

const c = 20;
console.log(c);
c = 30;
console.log(c);