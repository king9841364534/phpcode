function validateForm(){

	//reset all error message
	resetError();
	//get all form value using id
	let name = document.getElementById('name').value;
	let phone = document.getElementById('phone').value;
	let email = document.getElementById('email').value;
	let dob = document.getElementById('dob').value;
	let country = document.getElementById('country').value;
	let password = document.getElementById('password').value;
	//set error value 0
	let error = 0;
	//define regex for name
	let namepattern = /^[a-zA-Z\s]+$/
	let emailpattern = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}/
	let phonepattern = /^[9]{1}[7-8]{1}[0-9]{8}/
	let passwordpattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
	let datepattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

	//check blank value for email
	if (email == '') {
		//set error message using span #id
		document.getElementById('err_email').innerHTML = "Enter email";
		error++;
	}else if (!emailpattern.test(email)) {
		document.getElementById('err_email').innerText = "Emter valid email address";
		error++;
	}

	// for phone
	if (phone == '') {
		document.getElementById('err_phone').innerText = "Enter phone";
		error++;
	}else if (!phonepattern.test(phone)) {
		document.getElementById('err_phone').innerText = "Mobile number with 98 or 97 and remaing 8 digit with 0-9";
		error++;
	}

	//for name
	if (name == '') {
		document.getElementById('err_name').innerText = "Enter name";
		error++;
	}else if (!namepattern.test(name)) {
		document.getElementById('err_name').innerText = "Name must be character and space only";
		error++;
	}

	//for password
	if (password == '') {
		document.getElementById('err_password').innerText = "Enter password";
		error++;
	}else if (!passwordpattern.test(password)) {
		document.getElementById('err_password').innerText = "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character";
		error++;
	}

	//for date of birth
	if (dob == '') {
		document.getElementById('err_dob').innerText = "Enter date of birth";
		error++;
	}else if (!datepattern.test(dob)) {
		document.getElementById('err_dob').innerText = "Input a valid date [dd/mm/yyyy or dd-mm-yyyy format]";
		error++;
	}

	//for country
	if (country == '') {
		document.getElementById('err_country').innerText = "Select Country";
		error++;
	}

	//count error and return value
	if (error == 0) {
		return true;
	} else {
		return false;
	}
}

function resetError(){
	var errorList = document.getElementsByClassName('error');
	for (var i = 0; i < errorList.length; i++) {
		errorList[i].innerText = '';
	}
}