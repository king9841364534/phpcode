<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" href="owlcarousel/owl.theme.default.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="jquery.min.js"></script>
<script src="owlcarousel/owl.carousel.min.js"></script>
    <title></title>
</head>
<script type="text/javascript">
    $('.owl-carousel').owlCarousel({
    items:5,
    loop:true,
    margin:10,
    merge:true,
    responsive:{
        678:{
            mergeFit:true
        },
        1000:{
            mergeFit:false
        }
    }
});
</script>
<body>
<div class="owl-carousel owl-theme">
    <div class="item" data-merge="1"><h2>1</h2></div>
    <div class="item" data-merge="2"><h2>2</h2></div>
    <div class="item" data-merge="1"><h2>3</h2></div>
    <div class="item" data-merge="3"><h2>4</h2></div>
    <div class="item" data-merge="6"><h2>6</h2></div>
    <div class="item" data-merge="2"><h2>7</h2></div>
    <div class="item" data-merge="1"><h2>8</h2></div>
    <div class="item" data-merge="3"><h2>9</h2></div>
    <div class="item"><h2>10</h2></div>
    <div class="item"><h2>11</h2></div>
    <div class="item" data-merge="2"><h2>12</h2></div>
    <div class="item"><h2>13</h2></div>
    <div class="item"><h2>14</h2></div>
    <div class="item"><h2>15</h2></div>
</div>
</body>
</html>