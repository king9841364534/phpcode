<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
	</title>
</head>
<body>
<?php 
$students = [
	['S.N' => 1 , 'Name' => 'Ram' , 'Roll' => 1 , 'PHP' => 12 , 'Node' => 18 , 'API' => 25 , 'JS' => 30 , 'React' => 16 ],
	['S.N' => 2 , 'Name' => 'Sita' , 'Roll' => 2 , 'PHP' => 73 , 'Node' => 74 , 'API' => 55 , 'JS' => 66 , 'React' => 77 ],
	['S.N' => 3 , 'Name' => 'Gita' , 'Roll' => 9 , 'PHP' => 60 , 'Node' => 90 , 'API' => 95 , 'JS' => 89 , 'React' => 88 ],
	['S.N' => 4 , 'Name' => 'Rita' , 'Roll' => 8 , 'PHP' => 30 , 'Node' => 90 , 'API' => 60 , 'JS' => 80 , 'React' => 40 ]
];
?>
 <table border='1'>
	<tr>
		<th> S.N </th>
		<th> Name </th>
		<th> Roll </th>
		<th> PHP </th>
		<th> Node </th>
		<th> API </th>
		<th> JS </th>
		<th> React </th>
		<th> Total </th>
		<th> Percentage </th>
		<th> Result </th>
	</tr>
	<?php foreach ($students as $key => $student) { ?>
		<tr>
			<td><?php echo $student['S.N'] ?></td>
			<td><?php echo $student['Name'] ?></td>
			<td><?php echo $student['Roll'] ?></td>
			<td><?php echo $student['PHP'] ?></td>
			<td><?php echo $student['Node'] ?></td>
			<td><?php echo $student['API'] ?></td>
			<td><?php echo $student['JS'] ?></td>
			<td><?php echo $student['React'] ?></td>
			<td><?php $sum = $student['PHP'] + $student['Node'] + $student['API'] + $student['JS'] + $student['React']; 
				echo $sum; ?></td>
			<td><?php $per = $sum/5; echo $per . '%'; ?></td>
			<td><?php if( $student['PHP'] >= 50 && $student['Node'] >= 50 && $student['API'] >= 50 && $student['JS'] >= 50 && $student['React'] >= 50 ){
				echo 'Pass';
			}else{
				echo 'Fail';
			} ?></td>
		</tr>
<?php	} ?>
</body>
</html>