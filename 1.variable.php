<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<?php
		/*	
			Variable: Memory location to store value at runtime	
			Rules to create variable in php
			-Start with dollor sign '$'
			-Followed by character a to z or '_'.
			-We do not need data type for php variable	
			valid:$name,$name1,$_name,$firstname,$first_name,$firstName,$FirstName
			invalid: $1name,$first name.$1first_name,$first+name,$first-name,$$name				
		*/
	//create variable and store/assign value 
	$name="Ram Thapa";
	$address="Lalitpur";
	$email='milan@gmail.com';
	$phone='9869361818';
	echo $name;
	echo "<br>";
	//var_dump function display data with datatype in php
	var_dump($name);
	echo "<br>";
	echo 'My name is '. $name;
	echo '<br>My name is $name';
	echo "<br>My name is  $name";

	$a='hi';
	$hi="welcome to website";
	echo '<br>'.$$a;
	 ?>
<table border='1' width="30%">
		<tr>
			<th>Name</th>
			<td><?php echo $name; ?></td>
		</tr>
		<tr>
			<th>Address</th>
			<td><?php echo $address; ?></td>
		</tr>
		<tr>
			<th>Email</th>
			<td><?php echo $email; ?></td>
		</tr>
		<tr>
			<th>Phone No.</th>
			<td><?php echo $phone; ?></td>
		</tr>
	</table>
</body>
</html>