<?php 
  require_once 'header.php'; 
  $post->set('slug',$_GET['slug']);
  $post_details = $post->getPostDetailBySlug();
  if(!$post_details){
    header('location:index.php');
  }
  $comment->set('post_id',$post_details->id);
  if(isset($_POST['btnSubmit'])){
    $comment->set('name',$_POST['name']);
    $comment->set('email',$_POST['email']);
    $comment->set('comment',$_POST['comment']);
    $comment->set('comment_at',date('Y-m-d H:i:s'));
    $comment->saveCommentOfNews();
  }
  $comment_lists = $comment->displayAllCommentByPostId();
?>

    <!-- Page Content -->
    <!-- Banner Starts Here -->
    <div class="heading-page header-text">
      <section class="page-heading">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="text-content">
                <h4>Post Details</h4>
                <h2><?php echo $post_details->title ?></h2>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    
    <!-- Banner Ends Here -->

    <section class="blog-posts grid-system">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="all-blog-posts">
              <div class="row">
                <div class="col-lg-12">
                  <div class="blog-post">
                    <div class="blog-thumb">
                      <img src="Admin/img/<?php echo $post_details->image ?>" alt="">
                    </div>
                    <div class="down-content">
                      <ul class="post-info">
                        <li><a href="#"><?php echo ucfirst($post_details->name) ?></a></li>
                        <li><a href="#"><?php echo date("jS M Y", strtotime($post_details->created_at)) ?></a></li>
                        <li><a href="#"><?php echo count($comment_lists) ?> comments</a></li>
                      </ul>
                      <p><?php echo $post_details->description ?></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="sidebar-item comments">
                    <div class="sidebar-heading">
                      <?php if (count($comment_lists) == 0){ ?>
                      <h2 style='color:red'>No comments yet, Be a first commenter. </h2>
                      <?php }else { ?>
                        <h2><?php echo count($comment_lists) ?> comments</h2>
                      <?php } ?>
                    </div>
                    <div class="content">
                      <ul>
                        <?php foreach($comment_lists as $comment_list){ ?>
                        <li>
                          <div class="author-thumb">
                            <img src="admin/img/dummy2.png" alt="">
                          </div>
                          <div class="right-content">
                            <h4><?php echo $comment_list->name ?><span><?php echo date("jS M Y", strtotime($comment_list->comment_at)) ?></span></h4>
                            <p><?php echo $comment_list->comment ?>.</p>
                          </div>
                        </li><br>
                      <?php } ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="sidebar-item submit-comment">
                    <div class="sidebar-heading">
                      <h2>Your comment</h2>
                    </div>
                    <div class="content">
                      <form id="comment" action="" method="post">
                        <div class="row">
                          <div class="col-md-6 col-sm-12">
                            <fieldset>
                              <input name="name" type="text" id="name" placeholder="Your name" required="">
                            </fieldset>
                          </div>
                          <div class="col-md-6 col-sm-12">
                            <fieldset>
                              <input name="email" type="text" id="email" placeholder="Your email" required="">
                            </fieldset>
                          </div>
                          <div class="col-lg-12">
                            <fieldset>
                              <textarea name="comment" rows="6" id="comment" placeholder="Type your comment" required=""></textarea>
                            </fieldset>
                          </div>
                          <div class="col-lg-12">
                            <fieldset>
                              <button type="submit" id="form-submit" name="btnSubmit" class="main-button">Submit</button>
                            </fieldset>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="sidebar">
              <div class="row">
                <div class="col-lg-12">
                  <div class="sidebar-item search">
                    <form id="search_form" name="gs" method="GET" action="#">
                      <input type="text" name="q" class="searchText" placeholder="type to search..." autocomplete="on">
                    </form>
                  </div>
                </div>
                <?php foreach($menulists as $category_detail){ 
                  $post->set('category_id',$category_detail->id);
                  $details = $post->getPostsByCategory(3);
                  ?>
                  <div class="col-lg-12">
                    <div class="sidebar-item recent-posts">
                      <div class="sidebar-heading"><a href="category.php?slug=<?php echo $category_detail->slug ?>">
                        <h2><?php echo $category_detail->title ?></h2>
                      </div>
                      <div class="content">
                        <ul>
                          <?php if(count($details)== 0){ ?>
                            <li class="text-danger">No related post</li>
                          <?php }else { ?>
                          <?php foreach($details as $detail){ ?>
                          <li><a href="post-details.php?slug=<?php echo $detail->slug ?>">
                            <h5><?php echo $detail->title ?></h5>
                            <span><?php echo date("jS M Y", strtotime($detail->created_at)) ?></span>
                          </a></li>
                        <?php } }?>
                        </ul>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    
<?php require_once 'footer.php'; ?>