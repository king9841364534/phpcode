<?php 
require_once 'header.php';

$breaking_news = $post->displayBreakingNews();
$latestnews = $post->displayLatestNews();

?>


    <!-- Page Content -->
    <!-- Banner Starts Here -->
    <div class="main-banner header-text">
      <div class="container-fluid">
        <div class="owl-banner owl-carousel">
          <?php foreach($breaking_news as $breaking_new){ ?>
            <div class="item">
              <img src="admin/img/<?php echo $breaking_new->image ?>" height="298" alt="">
              <div class="item-content">
                <div class="main-content">
                  <div class="meta-category">
                    <span><?php echo $breaking_new->category ?></span>
                  </div>
                  <a href="post-details.php?slug=<?php echo $breaking_new->slug ?>"><h4><?php echo $breaking_new->short_description ?></h4></a>
                  <ul class="post-info">
                    <li><a href="#"><?php echo ucfirst($breaking_new->name) ?></a></li>
                    <li><a href="#"><?php echo date("jS M Y", strtotime($breaking_new->created_at)) ?></a></li>
                    <li><a href="#"><?php $comment->set('post_id',$breaking_new->id); echo count($comment->displayAllCommentByPostId()); ?> Comments</a></li>
                  </ul>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <!-- Banner Ends Here -->

    <section class="call-to-action">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="main-content">
              <div class="row">
                <div class="col-lg-12">
                  <h2 style="color: #f48840;">Latest News</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section class="blog-posts">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="all-blog-posts">
              <div class="row">
                <?php foreach ($latestnews as $latestnew) {?>
                <div class="col-lg-12">
                  <div class="blog-post">
                    <div class="blog-thumb">
                      <img src="admin/img/<?php echo $latestnew->image ?>" alt="">
                    </div>
                    <div class="down-content">
                      <span><?php echo $latestnew->category ?></span>
                      <a href="post-details.php?slug=<?php echo $latestnew->slug ?>"><h4><?php echo $latestnew->title?></h4></a>
                      <ul class="post-info">
                        <li><a href="#"><?php echo ucfirst($latestnew->name) ?></a></li>
                        <li><a href="#"><?php echo date("jS M Y", strtotime($latestnew->created_at)) ?></a></li>
                        <li><a href="#"><?php $comment->set('post_id',$latestnew->id); echo count($comment->displayAllCommentByPostId()); ?> Comments</a></li>
                      </ul>
                      <p><?php echo $breaking_new->short_description ?> </p>
                      <div class="post-options">
                        <div class="row">
                          <div class="col-6">
                            <ul class="post-tags">
                              <li><i class="fa fa-tags"></i></li>
                              <li><a href="post-details.php?slug=<?php echo $latestnew->slug ?>">Read More...</a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
                <div class="col-lg-12">
                  <div class="main-button">
                    <a href="all_post.php">View All Posts</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="sidebar">
              <div class="row">
                <div class="col-lg-12">
                  <div class="sidebar-item search">
                    <form id="search_form" name="gs" method="GET" action="#">
                      <input type="text" name="q" class="searchText" placeholder="type to search..." autocomplete="on">
                    </form>
                  </div>
                </div>
                <?php foreach($menulists as $category_detail){ 
                  $post->set('category_id',$category_detail->id);
                  $details = $post->getPostsByCategory(3);
                  ?>
                  <div class="col-lg-12">
                    <div class="sidebar-item recent-posts">
                      <div class="sidebar-heading"><a href="category.php?slug=<?php echo $category_detail->slug ?>">
                        <h2><?php echo $category_detail->title ?></h2>
                      </div>
                      <div class="content">
                        <ul>
                          <?php if(count($details)== 0){ ?>
                            <li class="text-danger">No related post</li>
                          <?php }else { ?>
                          <?php foreach($details as $detail){ ?>
                          <li><a href="post-details.php?slug=<?php echo $detail->slug ?>">
                            <h5><?php echo $detail->title ?></h5>
                            <span><?php echo date("jS M Y", strtotime($detail->created_at)) ?></span>
                          </a></li>
                        <?php } }?>
                        </ul>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    
<?php require_once 'footer.php' ?>