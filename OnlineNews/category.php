<?php 
require_once 'header.php'; 
$page = 1;
if(isset($_GET['page'])){
  $page = $_GET['page'];
}
$limit = 6;
$categories->set('slug',$_GET['slug']);
$cdetails = $categories->getCategoryDetailBySlug();
if(!$cdetails){
  header('location:index.php');
}
$post->set('category_id',$cdetails->id);
$all_posts = $post->getCategoryPostsWithPagination($page,$limit);
$getPostNo = $post->countAllCategoryPost();
?>

    <!-- Page Content -->
    <!-- Banner Starts Here -->
    <div class="heading-page header-text">
      <section class="page-heading">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="text-content">
                <h2><center>All Post of <?php echo $cdetails->title ?></center></h2>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    
    <!-- Banner Ends Here -->

    <section class="blog-posts grid-system">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="all-blog-posts">
              <div class="row">
                <?php foreach ($all_posts as $all_post) { ?>
                <div class="col-lg-4">
                  <div class="blog-post">
                    <div class="blog-thumb">
                      <img src="admin/img/<?php echo $all_post->image ?>" height='239px' alt="">
                    </div>
                    <div class="down-content">
                      <span><?php echo $all_post->category ?></span>
                      <a href="post-details.php?slug=<?php echo $all_post->slug ?>"><h4><?php echo $all_post->title ?></h4></a>
                      <ul class="post-info">
                        <li><a href="#"><?php echo ucfirst($all_post->name) ?></a></li>
                        <li><a href="#"><?php echo date("jS M Y", strtotime($all_post->created_at)) ?></a></li>
                        <li><a href="#"><?php $comment->set('post_id',$all_post->id); echo count($comment->displayAllCommentByPostId()); ?> Comments</a></li>
                      </ul>
                      <p><?php echo $all_post->short_description ?></p>
                      <div class="post-options">
                        <div class="row">
                          <div class="col-lg-12">
                            <ul class="post-tags">
                              <li><i class="fa fa-tags"></i></li>
                              <li><a href="post-details.php?slug=<?php echo $all_post->slug ?>">Read More...</a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
                <div class="col-lg-12">
                  <ul class="page-numbers">
                    <?php for($i = 0; $i < ceil($getPostNo->total_no_of_post/$limit); $i++){?>
                      <li class="<?php echo ($page==$i+1)?'active':'' ?>"><a href="category.php?slug=<?php echo $cdetails->slug ?>&page=<?php echo $i+1 ?>"><?php echo $i+1 ?></a></li>
                    <?php }?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    
<?php require_once 'footer.php'; ?>