<?php
$email = $password = ''; 

    if(isset($_POST['btnSubmit'])){
        $error = [];

        if(isset($_POST['email']) && !empty($_POST['email']) && trim($_POST['email'])){
            $email = $_POST['email'];
            if(!filter_var($email , FILTER_VALIDATE_EMAIL)){
                $error['email'] = "Please enter a valid email";
            }
        }else{
            $error['email'] = "Please enter a valid email";
        }

        if(isset($_POST['password']) && !empty($_POST['password'])){
            $password = $_POST['password'];
        }else{
            $error['password'] = "Please provide a password";
        }

        if(count($error)==0){
            require_once 'library/database.php';
            $db = new Database();
            $user = $db->checkAdminForLogin($email,md5($password)); 

            if($user){
                session_start();
                $_SESSION['admin_name'] = $user->name;
                $_SESSION['admin_email'] = $user->emial;
                $_SESSION['admin_id'] = $user->id;
                header('location:dashboard.php');
            }else{
                $error['loginfail'] =  'Login failed';
            }
        }
    }

    require_once 'library/Setting.php';
    $setting = new Setting(); 
    $info = $setting->getSetting();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>News Admin - Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">


    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="icon" type="image/ico" href="img/<?php echo $info[0]->favicon ?>" />

    <style type="text/css">
        .error {
            color: #ff5a02;
            font-size: 1rem;
            line-height: 1;
            width: 100%;
        }
    </style>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>

                                        <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                            <p class="alert alert-danger">Please login to continue</p>
                                          <?php } ?>

                                          <?php if (isset($error['loginfail'])) { ?>
                                            <p class="alert alert-danger"><?php  echo $error['loginfail']; ?></p>
                                          <?php } ?>
                                    </div>
                                    <form class="user" id="loginForm" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                                        <div class="form-group">
                                            <input type="email" name="email" value="<?php echo $email ?>" class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Enter Email Address..." required>
                                            <span class="error">
                                                <?php echo (isset($error['email'])?$error['email']:'') ?>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user"
                                                id="exampleInputPassword" placeholder="Password" required><i class="far fa-eye" id="togglePassword"></i>
                                            
                                            <span class="error">
                                                <?php echo (isset($error['password'])?$error['password']:'') ?>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="submit" name="btnSubmit" class="btn btn-primary btn-user btn-block">
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#loginForm").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 5
                    },
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    email: "Please enter a valid email address"
                }
            });
        });

        const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#exampleInputPassword");

        togglePassword.addEventListener("click", function () {
            // toggle the type attribute
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            
            // toggle the icon
            this.classList.toggle('fa-eye-slash');
        });
    </script>
    </script>

</body>

</html>