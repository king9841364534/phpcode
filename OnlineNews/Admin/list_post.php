<?php
    $title="List Post Admin-News";
    require_once 'header.php';
    require_once 'library/Post.php';
    $post = new Post();
    $post_list = $post->displayPostOfNews();
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Post</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">List / <a href="create_post.php" class="btn btn-success">Create</a></h6>
                                </div>
                                <div class="card-body">
                                    <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                            <p class="alert alert-danger">Invalid Request</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                                            <p class="alert alert-success">Delete Success</p>
                                          <?php } ?>
                                     <?php if (isset($_GET['msg']) && $_GET['msg'] == 3) { ?>
                                            <p class="alert alert-danger">Delete Failed</p>
                                          <?php } ?>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Category Name</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>Category Name</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($post_list as $in =>$query){ ?>
                                        <tr>
                                            <td><?php echo $in+1 ?></td>
                                            <td><?php echo $query->category ?></td>
                                            <td><?php echo $query->title ?></td>
                                            <td><?php if(!empty($query->image) && file_exists('img/' . $query->image)){ ?>
                                                <img src="img/<?php echo $query->image ?>" width='100px' >
                                            <?php } ?></td>
                                            <td><a href="view_post.php?id=<?php echo $query->id ?>" class="btn btn-info">View</a> <a href="edit_post.php?id=<?php echo $query->id ?>" class="btn btn-warning">Edit</a> <a href="delete_post.php?id=<?php echo $query->id ?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete')">Delete</a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  