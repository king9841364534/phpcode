<?php
    $title="View Post Admin-News";
    require_once 'header.php'; 
    require_once 'library/Post.php';
    $id = $_GET['id'];
	if(!is_numeric($id)){
		header('location:view_post.php?msg=1'); 
	}
    
	$post = new Post();
	$post->set('id',$id);
    $records = $post->getPostById();

?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Post</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">View / <a href="list_post.php" class="btn btn-success">List</a></h6>
                                </div>

                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Category</th>
                                            <td><?php echo $records[0]->category ?></td>
                                        </tr>

                                        <tr>
                                            <th>Title</th>
                                            <td><?php echo $records[0]->title ?></td>
                                        </tr>

                                        <tr>
                                            <th>Slug</th>
                                            <td><?php echo $records[0]->slug ?></td>
                                        </tr>

                                        <tr>
                                            <th>Image</th>
                                            <td><?php if(!empty($records[0]->image) && file_exists('img/' . $records[0]->image)){ ?>
                                                <img src="img/<?php echo $records[0]->image ?>" width='100px' >
                                            <?php } ?></td>
                                        </tr>

                                        <tr>
                                            <th>Short Description</th>
                                            <td><?php echo $records[0]->short_description ?></td>
                                        </tr>

                                        <tr>
                                            <th>Description</th>
                                            <td><?php echo $records[0]->description ?></td>
                                        </tr>

                                        <tr>
                                            <th>Created By</th>
                                            <td><?php echo ucfirst($records[0]->name) ?></td>
                                        </tr>

                                        <tr>
                                            <th>Created At</th>
                                            <td><?php echo $records[0]->created_at ?></td>
                                        </tr>

                                        <tr>
                                            <th>Updated By</th>
                                            <td><?php echo ucfirst($records[0]->uname) ?></td>
                                        </tr>

                                        <tr>
                                            <th>Updated At</th>
                                            <td><?php echo $records[0]->updated_at ?></td>
                                        </tr>
                                    </table>

                                    <a href="list_post.php" class="btn btn-info"><--Back</a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

