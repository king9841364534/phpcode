<?php
    $title="Create Post Admin-News";
    require_once 'header.php'; 
    require_once 'library/Category.php';
    require_once 'library/Database.php';
    $db = new Database();
    $database = $db->connectDB();
    $category = new Category();
    $category_list = $category->displayCategoryOfNews();
    if(isset($_POST['btnCreate'])){
        require_once 'library/Post.php';
        $post = new Post();
        $post->category_id = $_POST['category_id'];
        $post->title = $_POST['title'];
        $post->slug = $_POST['slug'];
        $post->breaking_key = $_POST['breaking_key'];
        $post->short_description = $_POST['short_description'];
        $post->description = $database->real_escape_string($_POST['description']); 
        $post->created_by = $_SESSION['admin_id']; 
        $post->created_at = date('Y-m-d H:i:s');
        if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
            $types= ['image/png','image/jpeg','image/jpg','image/gif'];
            if(in_array($_FILES['image']['type'],$types)){
                $image = uniqid() . '_' .$_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $image);
                $post->image = $image;
            }
        }

        $result = $post->savePostOfNews();
    }
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Post</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Create / <a href="list_post.php" class="btn btn-success">List</a></h6>
                                </div>

                                <div class="card-body">
                                    <?php 
                                        if (isset($result) && $result == true) { ?>
                                        <div class="alert alert-success">Post insert successful</div>
                                    <?php }elseif (isset($result) && $result == false) { ?>
                                        <div class="alert alert-danger">Post insert failed</div>
                                    <?php } ?>
                                   <form id="createCategory" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label for="category">Category:</label>
                                        <select class="form-control" name="category_id">
                                            <option value="">--Select Category--</option>
                                            <?php foreach($category_list as $category){ ?>
                                                <option value="<?php echo $category->id ?>"><?php echo $category->title ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">Title:</label>
                                        <input type="text" name="title" id="title" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="slug">Slug:</label>
                                        <input type="text" name="slug" id="slug" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Image:</label>
                                        <input type="file" accept="image/*" onchange="loadFile(event)" name="image" class="form-control" required>
                                        <img id="output" src="img/dummy.png" width="150px">
                                    </div>

                                    <div class="form-group">
                                        <label for="short_description">Short Description:</label>
                                        <textarea name="short_description" class="form-control" required></textarea> 
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description:</label>
                                        <textarea name="description" id="description" class="form-control" required></textarea> 
                                    </div>

                                    <div class="form-group">
                                        <label for="breaking_key">Breaking Key:</label>
                                        <input type="radio" name="breaking_key" value='1'> Active <input type="radio" name="breaking_key" value='0' checked> De-Active 
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="btnCreate" class="btn btn-success">
                                    </div>
                                </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

 <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

<!-- ckeditor-->
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#createCategory").validate({
                rules: {
                    image: {
                        required: true,
                        accept: "image/jpg,image/jpeg,image/png,image/gif",
                        filesize: 500000,
                    },
                    title: "required",
                    slug: "required",
                    description: "required",
                    short_description: "required",
                },
                messages: {
                    image: {
                        required: 'Please enter image', 
                        accept: 'Not an image!'
                    }
                }
            });
        });

        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
              var output = document.getElementById('output');
              output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        $("#title").keyup(function() {
          var Text = $(this).val();
          Text = Text.toLowerCase();
          Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
          $("#slug").val(Text);        
        });

        ClassicEditor
        .create( document.querySelector( '#description' ) )
        .catch( error => {
            console.error( error );
        } );
    </script>