<?php
    $title="Update Setting Admin-News";
    require_once 'library/Setting.php';
    require_once 'library/Database.php';
    $db = new Database();
    $database = $db->connectDB();
    $settings = new Setting();
    $records = $settings->getSetting();
    if(isset($_POST['btnUpdate'])){
        $settings->title = $database->real_escape_string($_POST['title']);
        $settings->slogan = $database->real_escape_string($_POST['slogan']);
        $settings->address = $database->real_escape_string($_POST['address']);
        $settings->email = $database->real_escape_string($_POST['email']);
        $settings->phone = $_POST['phone'];
        $settings->website = $database->real_escape_string($_POST['website']);
        $settings->google_map_link = $database->real_escape_string($_POST['google_map_link']);
        $settings->facebook_link = $database->real_escape_string($_POST['facebook_link']);
        $settings->twitter_link = $database->real_escape_string($_POST['twitter_link']);
        $settings->youtube_link = $database->real_escape_string($_POST['youtube_link']);
        $settings->instagram_link = $database->real_escape_string($_POST['instagram_link']);
        $settings->about_us = $database->real_escape_string($_POST['about_us']);
        $settings->privacy_policy = $database->real_escape_string($_POST['privacy_policy']);
        if(isset($_FILES['logo']) && $_FILES['logo']['error'] == 0){
            $types= ['image/png','image/jpeg','image/jpg','image/gif'];
            if(in_array($_FILES['logo']['type'],$types)){
                $image = $_FILES['logo']['name'];
                move_uploaded_file($_FILES['logo']['tmp_name'], 'img/' . $image);
                $settings->logo = $image;
            }
        }

        if(isset($_FILES['favicon']) && $_FILES['favicon']['error'] == 0){
            $types= ['image/png','image/jpeg','image/jpg','image/gif'];
            if(in_array($_FILES['favicon']['type'],$types)){
                unlink('img/' . $records[0]->favicon);
                $image = $_FILES['favicon']['name'];
                move_uploaded_file($_FILES['favicon']['tmp_name'], 'img/' . $image);
                $settings->favicon = $image;
            }
        }

        $result = $settings->updateSetting();
    }
    require_once 'header.php'; 
    
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Settings</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Site Setting</h6>
                                </div>

                                <div class="card-body">
                                    <?php 
                                        if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                                        <div class="alert alert-success">Setting Change</div>
                                    <?php }elseif (isset($result) && $result == false) { ?>
                                        <div class="alert alert-danger">Setting Changed Failed</div>
                                    <?php } ?>
                                   <form id="createSetting" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label for="title">Title:</label>
                                        <input type="text" name="title" id="title" value="<?php echo $records[0]->title ?>" class="form-control" pattern="[a-zA-Z0-9]+" minlength="2" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="slogan">Slogan:</label>
                                        <input type="text" name="slogan" id="slogan" value="<?php echo $records[0]->slogan ?>" class="form-control" pattern="[a-zA-Z0-9]+" minlength="4" required>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="logo">Logo:</label>
                                                <input type="file" accept="image/*" onchange="loadFile(event)" name="logo" class="form-control">
                                                    <img id="output" src="img/<?php echo $records[0]->logo ?>" width="100px">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="favicon">Favicon:</label>
                                                <input type="file" accept="image/*" onchange="loadFile2(event)" name="favicon" class="form-control">
                                                    <img id="output2" src="img/<?php echo $records[0]->favicon ?>" width="100px">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="address">Address:</label>
                                                <input type="text" name="address" value="<?php echo $records[0]->address ?>" class="form-control" required> 
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="email">Email:</label>
                                                <input type="text" name="email" value="<?php echo $records[0]->email ?>" class="form-control" required> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                             <div class="form-group">
                                                <label for="phone">Phone:</label>
                                                <input type="text" name="phone" id="phone" value="<?php echo $records[0]->phone ?>" class="form-control" required> 
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="website">Website:</label>
                                                <input type="text" name="website" value="<?php echo $records[0]->website ?>" class="form-control" required> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="google_map_link">Google Map:</label>
                                        <input type="text" name="google_map_link" value="<?php echo $records[0]->google_map_link ?>" class="form-control" required> 
                                    </div>

                                    <div class="form-group">
                                        <label for="about_us">About us:</label>
                                        <textarea name="about_us" id="about_us" class="form-control" required><?php echo $records[0]->about_us ?></textarea> 
                                    </div>

                                    <div class="form-group">
                                        <label for="privacy_policy">Privacy and Policy:</label>
                                        <textarea name="privacy_policy" id="privacy_policy" class="form-control" required><?php echo $records[0]->privacy_policy ?></textarea> 
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="facebook_link">Facebook:</label>
                                                <input type="text" name="facebook_link" value="<?php echo $records[0]->facebook_link ?>" class="form-control" required> 
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="twitter_link">Twitter:</label>
                                                <input type="text" name="twitter_link" value="<?php echo $records[0]->twitter_link ?>" class="form-control" required> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="youtube_link">YouTube:</label>
                                                <input type="text" name="youtube_link" value="<?php echo $records[0]->youtube_link ?>" class="form-control" required> 
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="instagram_link">Instagram:</label>
                                                <input type="text" name="instagram_link" value="<?php echo $records[0]->instagram_link ?>" class="form-control" required> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="btnUpdate" value="Update" class="btn btn-success">
                                    </div>
                                </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

 <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

<!-- ckeditor-->
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
           $("#createSetting").validate();
        });

        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
              var output = document.getElementById('output');
              output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        var loadFile2 = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
              var output = document.getElementById('output2');
              output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        ClassicEditor
        .create( document.querySelector( '#about_us' ) )
        .catch( error => {
            console.error( error );
        } );

        ClassicEditor
        .create( document.querySelector( '#privacy_policy' ) )
        .catch( error => {
            console.error( error );
        } );

    </script>