<?php
$id = $_GET['id'];
if(!is_numeric($id)){
	header('location:list_post.php?msg=1'); 
}
require_once 'library/Post.php';
$category = new Post();
$category->set('id',$id);
$records = $category->getPostById();
if(count($records) == 0){
	header('location:list_post.php?msg=1'); 
}
$msg = $category->deletePostById();
if(!empty($records[0]->image) && file_exists('img/' . $records[0]->image)){
	unlink('img/' . $records[0]->image);
}
if($msg == true) {
	header('location:list_post.php?msg=2'); 
}else{
	header('location:list_post.php?msg=3');
}
?>