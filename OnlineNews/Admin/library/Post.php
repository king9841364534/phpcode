<?php 
require_once 'database.php';

class Post extends database{
	public $category_id,$id,$title,$slug,$breaking_key,$image,$created_by,$created_at,$updated_by,$updated_at,$short_description,$description;

	function savePostOfNews(){
		$sql = "insert into posts(category_id,title,slug,image,breaking_key,short_description,description,created_by,created_at) values ('$this->category_id','$this->title','$this->slug','$this->image','$this->breaking_key','$this->short_description','$this->description','$this->created_by','$this->created_at')";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1 && $this->connection->insert_id > 0){
			return true;
		}else{
			return false;
		}
	}

	function displayPostOfNews(){
		$sql= "select p.*,c.title as category from posts p join categories c on p.category_id=c.id";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function getPostById(){
		$sql= "select p.*,c.title as category,a.name,u.name as uname from posts p join categories c on p.category_id=c.id left join admins a on p.created_by=a.id left join admins u on p.updated_by=u.id where p.id=$this->id";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows == 1){
			$data[] = $result->fetch_object();
		}
		return $data; 
	}

	function deletePostById(){
		$sql = "delete from posts where id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function updatePostOfNews(){
		if(empty($this->image)){
			$sql = "update posts set category_id='$this->category_id',title='$this->title',slug='$this->slug',breaking_key='$this->breaking_key',short_description='$this->short_description
		',description='$this->description',updated_by='$this->updated_by',updated_at='$this->updated_at' where id='$this->id'";
		}else{
			$sql = "update posts set category_id='$this->category_id',title='$this->title',slug='$this->slug',image='$this->image',breaking_key='$this->breaking_key',short_description='$this->short_description
			',description='$this->description',updated_by='$this->updated_by',updated_at='$this->updated_at' where id='$this->id'";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function displayBreakingNews(){
		$sql= "select p.*,c.title as category,u.name from posts p join categories c on p.category_id=c.id join admins u on p.created_by=u.id where p.breaking_key=1 order by p.created_at";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function displayLatestNews($limit=5){
		$sql= "select p.*,c.title as category,u.name from posts p join categories c on p.category_id=c.id join admins u on p.created_by=u.id order by p.created_at limit $limit";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function getPostsByCategory($limit=5){
		$sql= "select p.*,c.title as category,u.name from posts p join categories c on p.category_id=c.id join admins u on p.created_by=u.id where p.category_id=$this->category_id order by p.created_at limit $limit";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function getAllPostsWithPagination($page=1,$limit=6){
		$offset = ($page-1) * $limit;
		$sql= "select p.*,c.title as category,u.name from posts p join categories c on p.category_id=c.id join admins u on p.created_by=u.id order by p.created_at limit $limit offset $offset";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function countAllPost(){
		$sql= "select count(id) as total_no_of_post from posts";
		$this->connectDB();
		$result = $this->connection->query($sql);
		return $result->fetch_object();
	}

	function getCategoryPostsWithPagination($page=1,$limit=6){
		$offset = ($page-1) * $limit;
		$sql= "select p.*,c.title as category,u.name from posts p join categories c on p.category_id=c.id join admins u on p.created_by=u.id where p.category_id='$this->category_id' order by p.created_at limit $limit offset $offset";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function countAllCategoryPost(){
		$sql= "select count(id) as total_no_of_post from posts where category_id='$this->category_id'";
		$this->connectDB();
		$result = $this->connection->query($sql);
		return $result->fetch_object();
	}

	function getPostDetailBySlug(){
		$sql= "select p.*,a.name from posts p join admins a on p.created_by=a.id where slug='$this->slug'";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows > 0){
			return $result->fetch_object();
		}else{
			return false;
		} 
	}	

}

?>