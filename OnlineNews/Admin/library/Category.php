<?php 
require_once 'database.php';

class Category extends database{
	public $id,$title,$slug,$image,$rank,$created_by,$created_at;

	function saveCategoryOfNews(){
		$sql = "insert into categories(title,slug,image,rank,created_by,created_at) values ('$this->title','$this->slug','$this->image','$this->rank','$this->created_by','$this->created_at')";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1 && $this->connection->insert_id > 0){
			return true;
		}else{
			return false;
		}
	}

	function displayCategoryOfNews(){
		$sql= "SELECT * FROM categories";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function getCategoryById(){
		$sql= "select c.*,a.name,u.name as uname from categories c join admins a on c.created_by=a.id left join admins u on c.updated_by=u.id where c.id=$this->id";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows == 1){
			$data[] = $result->fetch_object();
		}return $data; 
	}

	function deleteCategoryById(){
		$sql = "delete from categories where id='$this->id'";
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function updateCategoryOfNews(){
		if(empty($this->image)){
			$sql = "update categories set title='$this->title',slug='$this->slug',rank='$this->rank',updated_by='$this->updated_by',updated_at='$this->updated_at' where id='$this->id'";
		}else{
			$sql = "update categories set title='$this->title',slug='$this->slug',image='$this->image',rank='$this->rank',updated_by='$this->updated_by',updated_at='$this->updated_at' where id='$this->id'";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	function getAllCategoryForMenu(){
		$sql= "SELECT * FROM categories order by rank";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows > 0){
			while ($row = $result->fetch_object()) {
				array_push($data, $row);			
			}
		}return $data; 
	}

	function getCategoryDetailBySlug(){
		$sql= "select * from categories where slug='$this->slug'";
		$this->connectDB();
		$result = $this->connection->query($sql);
		if($result->num_rows > 0){
			return $result->fetch_object();
		}else{
			return false;
		} 
	}	

}

?>