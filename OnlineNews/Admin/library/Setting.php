<?php 
require_once 'database.php';

class Setting extends database{
	public $id,$title,$slogan,$logo,$favicon,$address,$email,$about_us,$privacy_policy,$phone,$website,$google_map_link,$facebook_link,$youtube_link,$twitter_link,$instagram_link;

	function getSetting(){
		$sql= "select * from settings where id=1";
		$this->connectDB();
		$result = $this->connection->query($sql);
		$data = [];
		if($result->num_rows == 1){
			$data[] = $result->fetch_object();
		}return $data; 
	}
	
	function updateSetting(){
		if(empty($this->logo)){
			$sql = "update settings set title='$this->title',slogan='$this->slogan',favicon='$this->favicon',address='$this->address',email='$this->email',about_us='$this->about_us',privacy_policy='$this->privacy_policy',phone='$this->phone',website='$this->website',google_map_link='$this->google_map_link',facebook_link='$this->facebook_link',youtube_link='$this->youtube_link',twitter_link='$this->twitter_link',instagram_link='$this->instagram_link' where id=1";
		}else if(empty($this->favicon)){
			$sql = "update settings set title='$this->title',slogan='$this->slogan',logo='$this->logo',address='$this->address',email='$this->email',about_us='$this->about_us',privacy_policy='$this->privacy_policy',phone='$this->phone',website='$this->website',google_map_link='$this->google_map_link',facebook_link='$this->facebook_link',youtube_link='$this->youtube_link',twitter_link='$this->twitter_link',instagram_link='$this->instagram_link' where id=1";
		}else if(empty($this->icon) && empty($this->favicon)){
			$sql = "update settings set title='$this->title',slogan='$this->slogan',address='$this->address',email='$this->email',about_us='$this->about_us',privacy_policy='$this->privacy_policy',phone='$this->phone',website='$this->website',google_map_link='$this->google_map_link',facebook_link='$this->facebook_link',youtube_link='$this->youtube_link',twitter_link='$this->twitter_link',instagram_link='$this->instagram_link' where id=1";
		}else{
			$sql = "update settings set title='$this->title',slogan='$this->slogan',logo='$this->logo',favicon='$this->favicon',address='$this->address',email='$this->email',about_us='$this->about_us',privacy_policy='$this->privacy_policy',phone='$this->phone',website='$this->website',google_map_link='$this->google_map_link',facebook_link='$this->facebook_link',youtube_link='$this->youtube_link',twitter_link='$this->twitter_link',instagram_link='$this->instagram_link' where id=1";
		}
		$this->connectDB();
		$this->connection->query($sql);
		if($this->connection->affected_rows == 1){ 
			header('location:setting_site.php?msg=1');
		}else{
			return false;
		}
	}

}

?>