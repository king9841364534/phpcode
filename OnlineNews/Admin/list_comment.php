<?php
    $title="List Comment Admin-News";
    require_once 'header.php';
    require_once 'library/Comment.php';
    $comment = new Comment();
    $list = $comment->displayAllComment();  
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Comment</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">List</h6>
                                </div>
                                <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>News Title</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Comment</th>
                                            <th>Commented Date</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                             <th>SN</th>
                                            <th>News Title</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Comment</th>
                                            <th>Commented Date</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($list as $in =>$query){ ?>
                                        <tr>
                                            <td><?php echo $in+1 ?></td>
                                            <td><?php echo $query->post ?></td>
                                            <td><?php echo $query->name ?></td>
                                            <td><?php echo $query->email ?></td>
                                            <td><?php echo $query->comment ?></td>
                                            <td><?php echo $query->comment_at ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  