<?php
    $title="View Category Admin-News";
    require_once 'header.php'; 
    require_once 'library/Category.php';
    $id = $_GET['id'];
	if(!is_numeric($id)){
		header('location:view_category.php?msg=1'); 
	}
    
	$category = new Category();
	$category->set('id',$id);
    $records = $category->getCategoryById();

?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Category</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">View / <a href="list_category.php" class="btn btn-success">List</a></h6>
                                </div>

                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Title</th>
                                            <td><?php echo $records[0]->title ?></td>
                                        </tr>

                                        <tr>
                                            <th>Slug</th>
                                            <td><?php echo $records[0]->slug ?></td>
                                        </tr>

                                        <tr>
                                            <th>Image</th>
                                            <td><?php if(!empty($records[0]->image) && file_exists('img/' . $records[0]->image)){ ?>
                                                <img src="img/<?php echo $records[0]->image ?>" width='100px' >
                                            <?php } ?></td>
                                        </tr>

                                        <tr>
                                            <th>Created By</th>
                                            <td><?php echo ucfirst($records[0]->name) ?></td>
                                        </tr>

                                        <tr>
                                            <th>Created At</th>
                                            <td><?php echo $records[0]->created_at ?></td>
                                        </tr>

                                        <tr>
                                            <th>Updated By</th>
                                            <td><?php echo ucfirst($records[0]->uname) ?></td>
                                        </tr>
                                        
                                        <tr>
                                            <th>Updated At</th>
                                            <td><?php echo $records[0]->updated_at ?></td>
                                        </tr>
                                    </table>

                                    <a href="list_category.php" class="btn btn-info"><--Back</a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

