<?php
    $title="Create Category Admin-News";
    require_once 'header.php'; 
    if(isset($_POST['btnCreate'])){
        require_once 'library/Category.php';
        $category = new Category();
        $category->title = $_POST['title'];
        $category->slug = $_POST['slug'];
        $category->rank = $_POST['rank']; 
        $category->created_by = $_SESSION['admin_id']; 
        $category->created_at = date('Y-m-d H:i:s');
        if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
            $types= ['image/png','image/jpeg','image/jpg','image/gif'];
            if(in_array($_FILES['image']['type'],$types)){
                $image = $_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $image);
                $category->image = $image;
            }
        }

        $result = $category->saveCategoryOfNews();
    }
?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Category</h1>

                     <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Create / <a href="list_category.php" class="btn btn-success">List</a></h6>
                                </div>

                                <div class="card-body">
                                    <?php 
                                        if (isset($result) && $result == true) { ?>
                                        <div class="alert alert-success">Category insert successful</div>
                                    <?php }elseif (isset($result) && $result == false) { ?>
                                        <div class="alert alert-danger">Category insert failed</div>
                                    <?php } ?>
                                   <form id="createCategory" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label for="title">Title:</label>
                                        <input type="text" name="title" id="title" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="slug">Slug:</label>
                                        <input type="text" name="slug" id="slug" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Image:</label>
                                        <input type="file" accept="image/*" onchange="loadFile(event)" name="image" class="form-control" required>
                                        <img id="output" src="img/dummy.png" width="150px">
                                    </div>

                                    <div class="form-group">
                                        <label for="rank">Rank:</label>
                                        <input type="text" name="rank" class="form-control" required> 
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="btnCreate" class="btn btn-success">
                                    </div>
                                </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php require_once 'footer.php'; ?>  

 <!-- jquery validation-->
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#createCategory").validate({
                rules: {
                    image: {
                        required: true,
                        accept: "image/jpg,image/jpeg,image/png,image/gif",
                        filesize: 500000,
                    },
                    title: "required",
                    slug: "required",
                    rank: {
                        required: true,
                        number: true
                    }
                },
                messages: {
                    image: {
                        required: 'Please enter image', 
                        accept: 'Not an image!'
                    }
                }
            });
        });

        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
              var output = document.getElementById('output');
              output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        $("#title").keyup(function() {
          var Text = $(this).val();
          Text = Text.toLowerCase();
          Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
          $("#slug").val(Text);        
        });
    </script>