<?php require_once 'header.php'; ?>
    <!-- Page Content -->
    <!-- Banner Starts Here -->
    <div class="heading-page header-text">
      <section class="page-heading">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="text-content">
                <h4>contact us</h4>
                <h2>let’s stay in touch!</h2>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    
    <!-- Banner Ends Here -->


    <section class="contact-us">
      <div class="container">
        <div class="row">
        
          <div class="col-lg-12">
            <div class="down-contact">
              <div class="row">
                <div class="col-lg-8">
                  <div class="sidebar-item contact-form">
                    <div class="sidebar-heading">
                      <h2>Location</h2>
                    </div>
                      <div id="map">
                        <iframe src="<?php echo $info[0]->google_map_link ?>" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe>
                      </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="sidebar-item contact-information">
                    <div class="sidebar-heading">
                      <h2>contact information</h2>
                    </div>
                    <div class="content">
                      <ul>
                        <li>
                          <h5><?php echo $info[0]->phone ?></h5>
                          <span>PHONE NUMBER</span>
                        </li>
                        <li>
                          <h5><?php echo $info[0]->email ?></h5>
                          <span>EMAIL ADDRESS</span>
                        </li>
                        <li>
                          <h5><?php echo $info[0]->address ?></h5>
                          <span>ADDRESS</span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    
<?php require_once 'footer.php'; ?>