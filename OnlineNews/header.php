<?php 
  require_once 'Admin/library/Category.php';
  $categories = new Category();
  $menulists = $categories->getAllCategoryForMenu();

  require_once 'Admin/library/Post.php';
  $post = new Post(); 

  require_once 'Admin/library/Comment.php';
  $comment = new Comment(); 

  require_once 'Admin/library/Setting.php';
  $setting = new Setting(); 
  $info = $setting->getSetting();

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">

    <title><?php echo $info[0]->title ?></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-stand-blog.css">
    <link rel="stylesheet" href="assets/css/owl.css">

    <link rel="icon" type="image/ico" href="admin/img/<?php echo $info[0]->favicon ?>" />
<!--

TemplateMo 551 Stand Blog

https://templatemo.com/tm-551-stand-blog

-->
  </head>

  <body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->

    <!-- Header -->
    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.php"><h2><img src="admin/img/<?php echo $info[0]->logo ?>" height='50px'><?php echo $info[0]->title ?><em>.</em></h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li> 
              <?php foreach($menulists as $menu){ ?>
                <li class="nav-item">
                  <a class="nav-link" href="category.php?slug=<?php echo $menu->slug ?>"><?php echo $menu->title ?></a>
                </li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </nav>
    </header>