<footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <ul class="social-icons">
              <li><a href="<?php echo $info[0]->facebook_link ?>" target="_blank"><i class="fa fa-facebook"></i> Facebook</a></li>
              <li><a href="<?php echo $info[0]->twitter_link ?>" target="_blank"><i class="fa fa-twitter"></i> Twitter</a></li>
              <li><a href="<?php echo $info[0]->youtube_link ?>" target="_blank"><i class="fa fa-youtube"></i> YouTube</a></li>
              <li><a href="<?php echo $info[0]->instagram_link ?>" target="_blank"><i class="fa fa-instagram"></i> Instagram</a></li>
            </ul>
          </div>
          <div class="col-lg-3">
              <ul style="text-align : left;" >
                <li><h5 class="text-white mb-4">News</h5></li>
                <li><a class="btn btn-link text-white-50" href="index.php">Home</a></li>
                <?php foreach($menulists as $menu){ ?>
                <li class="nav-item">
                  <a class="btn btn-link text-white-50" href="category.php?slug=<?php echo $menu->slug ?>"><?php echo $menu->title ?></a>
                </li>
                <?php } ?>
              </ul>
          </div>
                    <div class="col-lg-3">
                        <ul style="text-align : left;" >
                        <li><h5 class="text-white mb-4">Quick Links</h5></li>
                        <li><a class="btn btn-link text-white-50" href="about_us.php">About Us</a></li>
                        <li><a class="btn btn-link text-white-50" href="contact.php">Contact Us</a></li>
                        <li><a class="btn btn-link text-white-50" href="privacypolicy.php">Privacy Policy</a></li>
                      </ul>
                    </div>
                    <div class="col-lg-3">
                        <h5 class="text-white mb-4">Contact</h5>
                        <p class="mb-2"><i class="fa fa-map-marker-alt me-3"><?php echo $info[0]->address ?></i></p>
                        <p class="mb-2"><i class="fa fa-phone-alt me-3"></i><?php echo $info[0]->phone ?></p>
                        <p class="mb-2"><i class="fa fa-envelope me-3"></i><?php echo $info[0]->email ?></p>
                    </div>
          <div class="col-lg-12">
            <div class="copyright-text">
              <p>&copy;Copyright <?php echo date('Y') ?> <?php echo $info[0]->title ?> <a href="<?php echo $info[0]->website ?>" class="btn btn-link text-white" target="_blank"><?php echo $info[0]->website ?></a> </p>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Additional Scripts -->
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/isotope.js"></script>
    <script src="assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html>