<?php 
$name = '';
$address = '';
$email = '';
$phone = '';
$dob = '';
$country = '';
$gender = '';
$image = '';
$bio = '';
if(isset($_POST['login'])){
	if(isset($_POST['Name']) && !empty($_POST['Name']) && trim($_POST['Name'])){
		$name = $_POST['Name'];
	}else{
		$errname = ' Enter Name ';
	}

	if(isset($_POST['Address']) && !empty($_POST['Address']) && trim($_POST['Address'])){
		$address = $_POST['Address'];
	}else{
		$erraddress = ' Enter Address ';
	}

	if(isset($_POST['Email']) && !empty($_POST['Email']) && trim($_POST['Email'])){
		$email = $_POST['Email'];
	}else{
		$erremail = ' Enter Email ';
	}

	if(isset($_POST['Phone']) && !empty($_POST['Phone']) && trim($_POST['Phone'])){
		$phone = $_POST['Phone'];
	}else{
		$errphone = ' Enter Phone ';
	}

	if(isset($_POST['DOB']) && !empty($_POST['DOB']) && trim($_POST['DOB'])){
		$dob = $_POST['DOB'];
	}else{
		$errdob = 'Enter Date Of Birth';
	}

	if(isset($_POST['Country']) && !empty($_POST['Country']) && trim($_POST['Country'])){
		$country = $_POST['Country'];
	}else{
		$errcountry = 'Select Country';
	}

	if(isset($_POST['Gender']) && !empty($_POST['Gender']) && trim($_POST['Gender'])){
		$gender = $_POST['Gender'];
	}else{
		$errgender = 'Enter Gender';
	}

	if(isset($_POST['Image']) && !empty($_POST['Image']) && trim($_POST['Image'])){
		$image = $_POST['Image'];
	}else{
		$errimage = 'Enter Image';
	}

	if(isset($_POST['Bio']) && !empty($_POST['Bio']) && trim($_POST['Bio'])){
		$bio = $_POST['Bio'];
	}else{
		$errbio = 'Enter Bio';
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
	<label>Name:</label>
	<input type="text" name="Name" value="<?php echo $name; ?>">
	<?php if(isset($errname)){ echo $errname; } ?>
	<br>
	<label>Address:</label>
	<input type="text" name="Address" value="<?php echo $address; ?>">
	<?php if(isset($erraddress)){ echo $erraddress; } ?>
	<br>
	<label>Email:</label>
	<input type="email" name="Email" value="<?php echo $email; ?>">
	<?php if(isset($erremail)){ echo $erremail; } ?>
	<br>
	<label>Phone:</label>
	<input type="Number" name="Phone" value="<?php echo $phone; ?>">
	<?php if(isset($errphone)){ echo $errphone; } ?>
	<br>
	<label>Date of Birth:</label>
	<input type="date" name="DOB" value="<?php echo $dob; ?>">
	<?php if(isset($errdob)){ echo $errdob; } ?>
	<br>
	<label>Country:</label>
	<select name="Country" value='<?php echo $country; ?>'>
	<option value=''>Select Country</option>
	<option <?php if($country=="Nepal"){echo "selected";}?>>Nepal</option>
	<option <?php if($country=="India"){echo "selected";}?>>India</option>
	<option <?php if($country=="China"){echo "selected";}?>>China</option>
	<option <?php if($country=="USA"){echo "selected";}?>>USA</option>
	</select>
	<?php if(isset($errcountry)){ echo $errcountry; } ?>
	<br>
	<label>Gender:</label>
	<input name="Gender" type="radio" value='M' <?php if ($gender == 'M') echo 'checked="checked"'; ?>/>Male <input name="Gender" type="radio" value='F' <?php if ($gender== 'F') echo 'checked="checked"';?> /> Female
	<?php if(isset($errgender)){ echo $errgender; } ?>
	<br>
	<label>Image:</label>
	<input type="File" name="image" value='<?php echo $image ?>'>
	<?php if(isset($errimage)){ echo $errimage; } ?>
	<br>
	<label>Bio:</label>
	<textarea name='Bio'><?php echo $bio; ?></textarea>
	<?php if(isset($errbio)){ echo $errbio; } ?>
	<br>
	<input type="submit" name="login">
</form>
</body>
</html>