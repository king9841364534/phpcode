<?php 

class Account{
	var $customer_name,$account_type,$balance;
	function setName($n){
		$this->customer_name = $n;
	}
	function setAccount($a){
		$this->account_type = $a;
	}
	function setBalance($b){
		$this->balance = $b;
	}
	function withdraw($n){
		$this->balance = $this->balance - $n;
		echo "Withdraw Amount = $n <br>";
	}
	function deposite($n){
		$this->balance = $this->balance + $n;
		echo "Deposite Amount = $n <br>";
	}
	function printAccountInfo(){
		echo "Customer $this->customer_name of $this->account_type account type has Rs. $this->balance balance<br>";
	}
}
$account1 = new Account();
$account1->setName("Ram");
$account1->setAccount("Current");
$account1->setBalance(23000);
$account1->printAccountInfo();
if(isset($_POST['Submit'])){
	if(empty($_POST['deposite']) && $_POST['withdraw'] != 0){
		$account1->withdraw($_POST['withdraw']);
		$account1->printAccountInfo();
	}
	if(empty($_POST['withdraw']) && $_POST['deposite'] != 0){
		$account1->deposite($_POST['deposite']);
		$account1->printAccountInfo();
	}
	if($_POST['deposite'] != 0 && $_POST['withdraw'] != 0 && !empty($_POST['deposite']) && !empty($_POST['withdraw'])){
		print_r($_POST);
		$account1->withdraw($_POST['withdraw']);
		$account1->printAccountInfo();
		$account1->deposite($_POST['deposite']);
		$account1->printAccountInfo();
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
Withdraw amount:<input type="Number" name="withdraw" min=0><br>
Deposite amount:<input type="Number" name="deposite" min=0><br>
<input type="submit" name="Submit">
</form>
</body>
</html>
