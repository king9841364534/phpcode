<?php
class Book{
	var $name , $no_of_page , $price;
	function setName($n){
		$this->name = $n;
	}
	function setNoOfPage($nop){
		$this->no_of_page = $nop;
	}
	function setPrice($p){
		$this->price = $p;
	}
	function printBookInfo(){
		echo "Book $this->name consiste of $this->no_of_page page and cost $this->price<br>";
	}
	function getName(){
		return $this->name;
	}
	function getNoOfPage(){
		return $this->no_of_page;
	}
	function getPrice(){
		return $this->price;
	}
} 
//objectname =  new classname();
//book1
$book1 = new Book();
/*print_r($book1);
//call method
$book1->printBookName();
*/
$book1->setName("PHP programming");
$book1->setNoOfPage(65);
$book1->setPrice(500);
$book1->printBookInfo();
$n = $book1->getName();
echo strtoupper($n).'<br>';
$nop = $book1->getNoOfPage();
echo $nop.'<br>';
$p = $book1->getPrice();
echo $p.'<br>';
//book2
$book2 = new Book();
$book2->setName("Programming with JavaScript");
$book2->setNoOfPage(165);
$book2->setPrice(1300);
$book2->printBookInfo();
?>