<?php 
class Person{
	var $name,$email,$address,$phone;
/*
	function setValue($var,$value){
		$this->$var = $value;
	}

	function getValue($var){
		return $this->$var;
	}
*/
	function __construct($n,$e,$a,$p){
		$this->name = $n;
		$this->email = $e;
		$this->address = $a;
		$this->phone = $p;
	}

	function getData(){
		return get_object_vars($this);
	}
}

class Students extends Person{
	var $roll,$course;

	function __construct($r,$n,$e,$c,$p,$a){
		$this->roll = $r;
		$this->course =$c;
		parent::__construct($n,$e,$a,$p); 
	}
}

$std = new Students(10,'Milan Karki','milan@gmail.com','BIM',9869361818,'Lalitpur');
$s = $std->getData();
print_r($s);
?>
