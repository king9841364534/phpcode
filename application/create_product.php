<?php 
  session_start();
  require_once 'check_session.php';
  require_once 'constant.php';

$title = $price = $cate_id = '';
if (isset($_POST['btnSave'])) {
  $error = [];

  if (isset($_POST['title']) && !empty($_POST['title']) && trim($_POST['title'])) {
    $title = $_POST['title'];
  } else {
    $error['title'] = 'Please enter title';
  }

  if (isset($_POST['price']) && !empty($_POST['price']) && trim($_POST['price'])) {
    $price = $_POST['price'];
  } else {
    $error['price'] = 'Please enter price';
  }

  if (isset($_POST['cate_id']) && !empty($_POST['cate_id']) && trim($_POST['cate_id'])) {
    $cate_id = $_POST['cate_id'];
  } else {
    $error['cate_id'] = 'Please enter title';
  }

  if (count($error) == 0) {
    //database connection
    //create database connection object
    try{
       $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

      //query to insert data
     $sql = "insert into products(title,price,category_id) values ('$title','$price','$cate_id')";

     //execute query
     if($connection->query($sql)){
        $msg =  'Category added successfully';
     }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }
    
   
  }

}

 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <link rel="stylesheet" type="text/css" href="../css/custom.css">

    <title>Create Product</title>
  </head>
  <body class="bg-dark">
      <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
              <div class="card bg-secondary">
                <div class="card-header bg-info">
                  Product Create
                </div>
                 <div class="card-body">
                  <?php 
                  if (isset($msg)) { ?>
                   <p class="alert alert-success"><?php  echo $msg; ?></p>
                 <?php }
                   ?>
                    <form accept="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $title ?>">
                            <span class="text-danger">
                              <?php echo (isset($error['title'])?$error['title']:'') ?>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="number" name="price" class="form-control" value="<?php echo $price ?>" min='0'>
                            <span class="text-danger">
                              <?php echo (isset($error['price'])?$error['price']:'') ?>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="cate_id">Category Id</label>
                            <input type="text" name="cate_id" class="form-control" value="<?php echo $cate_id ?>">
                            <span class="text-danger">
                              <?php echo (isset($error['cate_id'])?$error['cate_id']:'') ?>
                            </span>
                        </div>
                        <div class="form-group mt-2">
                          <input type="submit" name="btnSave" class="btn btn-success btn-lg">
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                  This is info
                </div>
            </div>
        </div>
    </div>
  </body>
</html>