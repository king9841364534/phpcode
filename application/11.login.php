<?php 
  $username = $password= "";
  if(isset($_POST['submit'])) {
    $err = [];
    if(isset($_POST['username']) && !empty($_POST['username']) && trim($_POST['username'])) {
      $username = $_POST['username'];
    } else {
      $err['name'] =  "Enter username";
    }

    if(isset($_POST['password']) && !empty($_POST['password'])){
      $password = $_POST['password'];
    } else {
      $err['password'] = "Enter password";
    }
    if (count($err) == 0) {
      if ($username == 'admin' && $password == 'admin123') {

        //start sessoin
        session_start();
        //store data into session
        $_SESSION['username'] = $username;

       //redirect to dashboard
        header('location:dashboard.php');
      } else {
        $error =  'Login failed';
      }
    }
  }
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/custom.css">

    <title>Login</title>
  </head>
  <body class="bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5">
              <div class="card bg-secondary">
                <div class="card-header bg-info">
                  Login Form
                </div>
                <div class="card-body">
                  
                   <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                    <p class="alert alert-danger">Please login to continue</p>
                  <?php } ?>

                  <?php if (isset($error)) { ?>
                    <p class="alert alert-danger"><?php  echo $error; ?></p>
                  <?php } ?>

                  <?php if (isset($success)) { ?>
                    <p class="alert alert-success"><?php  echo $success; ?></p>
                  <?php } ?>
                  <form action= "<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form" >
                    <div class="form-group">
                       <label class="form-label">Name</label>
                      <input type = "text" name = "username" value = "<?php echo $username; ?>" class="form-control">
                      <span class="text-danger">
                    <?php if(isset($err['name'])){ echo $err['name']; } ?>
                  </span>
                    </div>
                    <div class="form-group mt-2">
                      <label class="form-label">Password</label>
                      <input class="form-control" type = "password" name = "password" > 
                      <span class="text-danger">
                      <?php if(isset($err['password'])){ echo $err['password']; } ?>
                      </span>
                    </div>
                    <div class="form-group mt-2">
                      <input type = "submit" name = "submit" class="btn btn-success btn-lg" value="Login">
                    </div>
                </form>
              </div>
              </div>
            </div>
        </div>
    </div>
  </body>
</html>