<?php 
  session_start();
  require_once 'check_session.php';
  require_once 'constant.php';
    try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to select data
      $sql = "select * from categories";
      //exceute query and get result object
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($data, $row);
        }
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
      }
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <link rel="stylesheet" type="text/css" href="../css/custom.css">

    <title>Category List</title>
  </head>
  <body class="bg-dark">
      <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
              <div class="card bg-secondary">
                <div class="card-header bg-info">
                  Category List
                </div>
                <div class="card-body">
                  <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                   <p class="alert alert-success">Category Deleted Successfully</p>
                 <?php } ?>
                 <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                   <p class="alert alert-success">Category Delete Failed</p>
                 <?php } ?>
                   <table class="table table-bordered">
                     <thead>
                       <tr>
                         <th>SN</th>
                         <th>ID</th>
                         <th>Title</th>
                         <th>Action</th>
                       </tr>
                     </thead>
                     <tbody>
                      <?php foreach($data as $in => $record){ ?>
                       <tr>
                        <td><?php echo $in+1  ?></td>
                         <td><?php echo $record->id ?></td>
                         <td><?php echo $record->title ?></td>
                         <td>
                          <a href="edit_category.php" class="btn btn-warning">Edit</a>
                          <a href="delete_categories.php?id=<?php echo $record->id ?>" class="btn btn-danger">Delete</a>
                         </td>
                       </tr>
                     <?php } ?>
                     </tbody>
                   </table>
                </div>
                <div class="card-footer">
                  This is info
                </div>
            </div>
        </div>
    </div>
  </body>
</html>