<?php 
	$username = $address = $email = $phone = $dob =  $country = $gender = $bio = $image = "";
	if(isset($_POST['submit'])) {
		if(isset($_POST['username']) && !empty($_POST['username']) && trim($_POST['username'])) {
			$username = $_POST['username'];
		} else {
			$errname =  "Enter username";
		}

		if(isset($_POST['address']) && !empty($_POST['address']) && trim($_POST['address'])){
			$address = $_POST['address'];
		} else {
			$erraddress = "Enter address";
		}

		if(isset($_POST['email']) && !empty($_POST['email']) && trim($_POST['email'])){
			$email = $_POST['email'];
		} else {
			$erremail = "Enter email";
		}

		if(isset($_POST['phone']) && !empty($_POST['phone']) && trim($_POST['phone'])){
			$phone = $_POST['phone'];
		} else{
			$errphone = "Enter phone";
		}

		if(isset($_POST['dob']) && !empty($_POST['dob']) && trim($_POST['dob'])){
			$dob = $_POST['dob'];
		} else {
			$errdob = "Enter date of birth";
		}

		if(isset($_POST['country']) && !empty($_POST['country'])){
			$country = $_POST['country'];
		} else {
			$errcountry = "Select country";
		}

		if(isset($_POST['gender']) && !empty($_POST['gender'])){
			$gender = $_POST['gender'];
		} else {
			$errgender = "Enter gender";
		}

		if(isset($_POST['image']) && !empty($_POST['image'])){
			$image = $_POST['image'];
		} else {
			$errimage = "Choose image";
		}

		if(isset($_POST['bio']) && !empty($_POST['bio']) && trim($_POST['bio'])){
			$bio = $_POST['bio'];
		} else {
			$errbio = "Enter bio";
		}
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<form action= "<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" >
		<label>Name</label>
		<input type = "text" name = "username" value = "<?php echo $username; ?>">
		<?php if(isset($errname)){ echo $errname; } ?><br>
		<label>Address</label>
		<input type = "text" name = "address" value = "<?php echo $address; ?>" > 
		<?php if(isset($erraddress)){ echo $erraddress; } ?><br>
		<label>Email</label>
		<input type = "text" name = "email" value = "<?php echo $email; ?>" id="email" >
		<span id="email_msg"></span>
		<?php if(isset($erremail)){ echo $erremail; } ?><br>
		<label>Phone</label>
		<input type = "number" name = "phone" value = "<?php echo $phone; ?>" >
		<?php if(isset($errphone)){ echo $errphone;} ?><br>
		<label>Date Of Birth</label>
		<input type = "date" name = "dob" value="<?php echo $dob; ?>" >
		<?php if(isset($errdob)){ echo $errdob; } ?><br>
		<label>Country</label>
		<select name = "country">
			<option value="">Select Country</option>
			<option value="np" <?php echo ($country == 'np') ?'selected':'' ?>>Nepal</option>
			<option value="in" <?php echo ($country == 'in') ?'selected':'' ?>>India</option>
			<option value="ch" <?php echo ($country == 'ch') ?'selected':'' ?>>China</option>
		</select>
		<?php if(isset($errcountry)){ echo $errcountry; } ?>
		<br>
		<label>Gender</label>
		<input <?php echo ($gender == "Male")? "checked":''; ?> type = "radio" name = "gender" value = "Male">Male
		<input <?php echo ($gender == "Female") ?"checked":''; ?> type="radio" name="gender" value="Female">Female
		<?php if(isset($errgender)){ echo $errgender;} ?>
		<br>
		<label>Image</label>
		<input type = "file" name = "image" > <?php echo $image; ?>
		<?php if(isset($errimage)){ echo $errimage; } ?><br>
		<label>Bio</label>
		<textarea name = "bio"><?php echo $bio ?></textarea>
		<?php if(isset($errbio)){ echo $errbio;} ?><br>
		<input type = "submit" name = "submit" id="sub">
	</form>
<script type="text/javascript" src="javascript/js/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#email').keyup(function(){
			let email = $(this).val();
			$.ajax({
				url: 'check_email.php',
				data:{'em' : email},
				method:'post',
				dataType:'text',
				success:function(resp){
					if (resp == 1) {
						$('#sub'). prop('disabled', true);
						$('#email_msg').text('Email already exists');
					} else {
						$('#sub'). prop('disabled', false);
						$('#email_msg').text('Email available');
					}
				}
			});
		});
	});
</script>
</body>
</html>

